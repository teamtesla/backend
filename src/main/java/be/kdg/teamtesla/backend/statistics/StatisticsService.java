package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StatisticsService {
    private final StatisticsRepo statisticsRepo;

    public StatisticsService(StatisticsRepo statisticsRepo) {
        this.statisticsRepo = statisticsRepo;
    }


    public List<StatisticsUno> getStats() {
        return statisticsRepo.findAll();
    }

    public StatisticsUno getUserStats(UnoUser user) {
        return statisticsRepo.findByUser(user);
    }
}
