package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.security.model.UnoUser;

import javax.persistence.*;

@Entity
public class StatisticsUno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long statsId;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private UnoUser user;

    private int gamesPlayed;
    private int gamesWon;
    private int pointsEarned;
    private int maximumPoints;
    private int averagePoints;


    public StatisticsUno(UnoUser user) {
        this.user = user;
        pointsEarned=0;
        gamesPlayed = 0;
        gamesWon = 0;
        maximumPoints=0;
        averagePoints=0;
    }

    public StatisticsUno() {
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void addPoints(int points){
        if (points>maximumPoints){this.maximumPoints=points;
        }
        this.pointsEarned=this.pointsEarned+points;
    }

    private void setAveragePoints() {
        this.averagePoints=pointsEarned/gamesPlayed;
    }

    public void wonGame(){
        this.gamesWon++;
        setAveragePoints();
    }

    public void playedGame(){
        this.gamesPlayed++;
        setAveragePoints();
    }

    public int getPointsEarned() {
        return pointsEarned;
    }

    public int getMaximumPoints() {
        return maximumPoints;
    }

    public int getAveragePoints() {
        return averagePoints;
    }

    public UnoUser getUser() {
        return user;
    }

    public void setUser(UnoUser user) {
        this.user = user;
    }
}
