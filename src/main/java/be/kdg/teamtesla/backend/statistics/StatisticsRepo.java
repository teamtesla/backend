package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatisticsRepo extends JpaRepository<StatisticsUno, Long> {
    StatisticsUno findByUser(UnoUser user);
}
