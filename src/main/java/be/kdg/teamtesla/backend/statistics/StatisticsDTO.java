package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;

public class StatisticsDTO {
    private long statsId;
    private UnoUserDTO user;
    private int gamesPlayed;
    private int gamesWon;
    private int pointsEarned;

    public long getStatsId() {
        return statsId;
    }

    public void setStatsId(long statsId) {
        this.statsId = statsId;
    }

    public UnoUserDTO getUser() {
        return user;
    }

    public void setUser(UnoUserDTO user) {
        this.user = user;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(int pointsEarned) {
        this.pointsEarned = pointsEarned;
    }
}
