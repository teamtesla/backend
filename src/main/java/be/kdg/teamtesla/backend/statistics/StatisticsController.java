package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Type;
import java.util.List;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {
    private WebsocketDispatcher websocketDispatcher;
    private final StatisticsService statsService;
    private final UnoUserService userService;

    public StatisticsController(WebsocketDispatcher websocketDispatcher, StatisticsService statsService, UnoUserService userService) {
        this.websocketDispatcher = websocketDispatcher;
        this.statsService = statsService;
        this.userService = userService;
        createSubscriptions();
    }

    private void createSubscriptions(){
        websocketDispatcher.SubscribeOnTopic("statistics/getpersonal", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if(wsm.getUser() != null) {
                    UnoUser unoUser = wsm.getUser();
                    StatisticsUno statisticsUno = statsService.getUserStats(unoUser);
                    StatisticsDTO statisticsDTO = new ModelMapper().map(statisticsUno,StatisticsDTO.class);
                    websocketDispatcher.SendMessageToUser(wsm.getUser(), "statistics/getpersonal", statisticsDTO);
                }
            }
        }, String.class);
        websocketDispatcher.SubscribeOnTopic("statistics/users", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if(wsm.getUser() != null){
                    List<StatisticsUno> statisticsUno = statsService.getStats();
                    Type listType = new TypeToken<List<StatisticsDTO>>() {}.getType();
                    List<StatisticsDTO> statisticsDTOS = new ModelMapper().map(statisticsUno, listType);
                    websocketDispatcher.SendMessageToUser(wsm.getUser(), "statistics/users", statisticsDTOS);
                }
            }
        }, String.class);
    }
}
