package be.kdg.teamtesla.backend.ai;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;

public class Turn {
    private Card card;
    private Color choice;

    public Turn() {
    }

    public Turn(Card card, Color choice) {
        this.card = card;
        this.choice = choice;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Color getChoice() {
        return choice;
    }

    public void setChoice(Color choice) {
        this.choice = choice;
    }
}
