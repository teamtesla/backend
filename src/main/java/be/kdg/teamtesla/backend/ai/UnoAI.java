package be.kdg.teamtesla.backend.ai;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.card.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class UnoAI {
    public Turn playTurn(Card topCard, List<Card> hand, Color chosenColor) {
        List<Card> playableCards = playableCards(hand,topCard,chosenColor);

        if (playableCards.size() > 0) {
            Card chosenCard;
            if (playableCards.size() > 1) {
                chosenCard = chooseCard(playableCards);
            } else {
                chosenCard = playableCards.get(0);
            }
            return playCard(chosenCard,hand,topCard,chosenColor);
        } else return new Turn();
    }

    public List<Card> playableCards(List<Card> hand, Card topCard, Color chosenColor) {
        List<Card> playableCards = new ArrayList<>();
        int add4 = 0;
        for (Card card:hand) {
            if (card.getColor().equals(Color.BLACK)) {
                if (card.getValue().equals(Value.TAKEFOUR)) {
                    add4++;
                }
                playableCards.add(card);
            }
            else if (topCard.getColor().equals(Color.BLACK)) {
                if (chosenColor.equals(card.getColor())) {
                    playableCards.add(card);
                }
            }
            else if (card.getColor().equals(topCard.getColor()) || card.getValue().equals(topCard.getValue())) {
                playableCards.add(card);
            }
        }
        if (playableCards.size() > add4) {
            playableCards.removeIf(card -> card.getValue().equals(Value.TAKEFOUR));
        }
        return playableCards;
    }

    public Card chooseCard(List<Card> playableCards) {
        if (playableCards.stream().allMatch(card -> card.getColor().equals(Color.BLACK))) {
            return playableCards.get(0);
        } else playableCards.removeIf(card -> card.getColor().equals(Color.BLACK));
        if (playableCards.stream().allMatch(card -> card.getValue().equals(Value.TAKETWO))) {
            return playableCards.get(0);
        } else playableCards.removeIf(card -> card.getValue().equals(Value.TAKETWO));
        if (playableCards.stream().allMatch(card -> card.getValue().equals(Value.SKIP))) {
            return playableCards.get(0);
        } else playableCards.removeIf(card -> card.getValue().equals(Value.SKIP));
        if (playableCards.stream().allMatch(card -> card.getValue().equals(Value.REVERSE))) {
            return playableCards.get(0);
        } else playableCards.removeIf(card -> card.getValue().equals(Value.REVERSE));
        if (playableCards.stream().allMatch(card -> card.getValue().equals(Value.ZERO))) {
            return playableCards.get(0);
        } else playableCards.removeIf(card -> card.getValue().equals(Value.ZERO));
        Random random = new Random();
        return playableCards.get(random.nextInt(playableCards.size()));
    }

    public Turn playCard(Card playableCard, List<Card> hand, Card topCard, Color chosenColor) {
        Turn turn = new Turn();
        if (playableCard.getColor().equals(Color.BLACK)) {
            turn.setChoice(playBlackCard(hand, topCard, chosenColor));
        }
        turn.setCard(playableCard);
        return turn;
    }

    public Color playBlackCard(List<Card> hand, Card topCard, Color chosenColor) {
        if (hand.stream().allMatch(card -> card.getColor().equals(Color.BLACK))) {
            if (!topCard.getColor().equals(Color.BLACK)) {
                return chooseColor(topCard.getColor(),null);
            } else {
                return chooseColor(chosenColor,null);
            }
        } else {
            List<Color> colors = hand.stream().map(Card::getColor).distinct().collect(Collectors.toList());
            colors.removeIf(color -> color.equals(Color.BLACK));
            if (topCard.getColor().equals(Color.BLACK)) {
                colors.removeIf(color -> color.equals(chosenColor));
            }
            if (colors.size() == 0) {
                return chooseColor(chosenColor,null);
            } else {
                return chooseColor(null,colors);
            }
        }
    }

    public Color chooseColor(Color color, List<Color> colorList) {
        if (colorList == null) {
            colorList = new ArrayList<>(Arrays.asList(Color.values()));
            colorList.removeIf(color1 -> color1.equals(Color.BLACK));
        }
        if (color != null) {
            colorList.remove(color);
        }
        Random random = new Random();
        return colorList.get(random.nextInt(colorList.size()));
    }
}
