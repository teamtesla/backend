package be.kdg.teamtesla.backend.chat;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;

import javax.persistence.*;

@Entity
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String message;
    @OneToOne
    private UnoUser sender;
    @ManyToOne
    private Lobby lobby;

    public Chat(String message, UnoUser sender, Lobby lobby) {
        this.message = message;
        this.sender = sender;
        this.lobby = lobby;
    }
    public Chat(){

    }

    public long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnoUser getSender() {
        return sender;
    }

    public void setSender(UnoUser sender) {
        this.sender = sender;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }
}
