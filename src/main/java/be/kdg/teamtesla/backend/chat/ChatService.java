package be.kdg.teamtesla.backend.chat;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ChatService {
    private ChatRepo chatRepo;

    public ChatService(ChatRepo chatRepo) {
        this.chatRepo = chatRepo;
    }

    public void saveChat(Chat chat){
        chatRepo.save(chat);
    }

    public List<Chat> findChatByLobby(Lobby lobby){
        return chatRepo.findByLobby(lobby);
    }
}
