package be.kdg.teamtesla.backend.chat.dto;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;

public class ChatMessageDTO {
    private String message;
    private long lobbyId;
    private UnoUserDTO sender;

    public ChatMessageDTO(String message, long lobbyId) {
        this.message = message;
        this.lobbyId = lobbyId;
    }

    public ChatMessageDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(long lobbyId) {
        this.lobbyId = lobbyId;
    }

    public UnoUserDTO getSender() {
        return sender;
    }

    public void setSender(UnoUserDTO sender) {
        this.sender = sender;
    }
}
