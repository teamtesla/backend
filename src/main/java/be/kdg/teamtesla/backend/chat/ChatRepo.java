package be.kdg.teamtesla.backend.chat;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatRepo extends JpaRepository<Chat, Long> {
    List<Chat> findByLobby(Lobby lobby);
}
