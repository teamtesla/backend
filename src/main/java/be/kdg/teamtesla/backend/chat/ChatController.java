package be.kdg.teamtesla.backend.chat;

import be.kdg.teamtesla.backend.chat.dto.ChatMessageDTO;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.lobby.dto.LobbyDTO;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;

@Controller
@Transactional
public class ChatController {

    private WebsocketDispatcher dispatcher;
    private LobbyService lobbyService;
    private ChatService chatService;

    public ChatController(WebsocketDispatcher dispatcher, LobbyService lobbyService, ChatService chatService) {
        this.dispatcher = dispatcher;
        this.lobbyService = lobbyService;
        this.chatService = chatService;
        doSubscribtions();
    }

    private void doSubscribtions(){
        dispatcher.SubscribeOnTopic("chat/message", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                UnoUser sender = wsm.getUser();
                Lobby lobby = sender.getCurrentLobby();
                Chat chat = new Chat((String) wsm.getObject(), sender, lobby);
                chatService.saveChat(chat);
                ChatMessageDTO chatMessageDTO = new ModelMapper().map(chat,ChatMessageDTO.class);
                lobby.getUnoUsers().forEach(unoUser -> {
                    dispatcher.SendMessageToUser(unoUser,"chat/message", chatMessageDTO);
                });
            }
        }, String.class);

        dispatcher.SubscribeOnTopic("chat/history", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                List<Chat> chats = chatService.findChatByLobby(wsm.getUser().getCurrentLobby());
                Lobby lobby = wsm.getUser().getCurrentLobby();
                Type listType = new TypeToken<List<ChatMessageDTO>>() {}.getType();

                List<ChatMessageDTO> chatMessageDTOS = new ModelMapper().map(chats,listType);
                lobby.getUnoUsers().forEach(unoUser -> {
                    dispatcher.SendMessageToUser(unoUser, "chat/history", chatMessageDTOS);
                });
            }
        }, String.class);
    }
}
