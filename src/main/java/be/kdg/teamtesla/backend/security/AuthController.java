package be.kdg.teamtesla.backend.security;

import be.kdg.teamtesla.backend.security.error.BadRequestException;
import be.kdg.teamtesla.backend.security.error.InvalidVerificationTokenException;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.model.VerificationToken;
import be.kdg.teamtesla.backend.security.payload.ApiResponse;
import be.kdg.teamtesla.backend.security.payload.AuthResponse;
import be.kdg.teamtesla.backend.security.payload.LoginRequest;
import be.kdg.teamtesla.backend.security.payload.UserDTO;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import be.kdg.teamtesla.backend.security.registration.OnRegistrationCompleteEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.UUID;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private UserRepo userRepo;
    private UserDetailServiceImpl userDetailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private ApplicationEventPublisher applicationEventPublisher;
    private JavaMailSender mailSender;
    private Environment env;
    private AuthenticationManager authenticationManager;
    private TokenProvider tokenProvider;

    public AuthController(UserRepo userRepo, UserDetailServiceImpl userDetailService, BCryptPasswordEncoder bCryptPasswordEncoder, ApplicationEventPublisher applicationEventPublisher, JavaMailSender mailSender, Environment env, AuthenticationManager authenticationManager, TokenProvider tokenProvider) {
        this.userRepo = userRepo;
        this.userDetailService = userDetailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.applicationEventPublisher = applicationEventPublisher;
        this.mailSender = mailSender;
        this.env = env;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }
    @GetMapping("/getUUID")
    public ResponseEntity<?> getUUID(@RequestHeader final String Authorization){
        ObjectMapper mapper = new ObjectMapper();
        String token = Authorization.replace("Bearer ", "");
        long id = tokenProvider.getUserIdFromToken(token);
        UnoUser currentUser = userRepo.findById(id);
        try {
            return ResponseEntity.ok(mapper.writeValueAsString(currentUser.getUuid()));
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserDTO signUpRequest, HttpServletRequest request) {
        if(userRepo.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException("Email address already in use.");
        }

        // Creating user's account
        UnoUser user = new UnoUser();
        user.setUsername(signUpRequest.getUsername());
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(signUpRequest.getPassword());
        user.setProvider("local");
        user.setUuid(UUID.randomUUID().toString());

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        UnoUser result = userRepo.save(user);

        String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(result,request.getLocale(),appUrl));

        return ResponseEntity.ok(new ApiResponse(true, "User registered successfully@"));
    }

    @PostMapping("/registrationConfirm")
    @ResponseBody
    public ResponseEntity<?> confirmRegistration(@RequestBody final String token) {
        final String result = userDetailService.validateVerificationToken(token);
        if (result.equals("valid")) {
            final UnoUser user = userDetailService.getUser(token);
             String authToken = authWithoutPassword(user);
            return ResponseEntity.ok(new AuthResponse(authToken));
        }
        throw new InvalidVerificationTokenException(token);
    }

    @PostMapping("/verifytoken")
    @ResponseBody
    public ResponseEntity<?> verifyToken(@RequestBody final String token) {
        return ResponseEntity.ok(new ApiResponse(true, "Token successful"));
    }


    @PostMapping("/resendRegistrationToken")
    @ResponseBody
    public ResponseEntity<?> resendRegistrationToken(final HttpServletRequest request, @RequestBody final String existingToken) {
        final VerificationToken newToken = userDetailService.generateNewVerificationToken(existingToken);
        final UnoUser user = userDetailService.getUser(newToken.getToken());
        mailSender.send(constructResendVerificationTokenEmail(getAppUrl(request), newToken, user));
        return ResponseEntity.ok(new ApiResponse(true, "New verification email"));
    }

    private String authWithoutPassword(UnoUser user) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.createToken(authentication);
    }

    private SimpleMailMessage constructResendVerificationTokenEmail(final String contextPath, final VerificationToken newToken, final UnoUser user) {
        final String confirmationUrl = Constants.FRONTEND_URL + "verifyuser/" + newToken.getToken();
        return constructEmail("Resend Registration Token", "Here is your new registration link:" + " \r\n" + confirmationUrl, user);
    }

    private SimpleMailMessage constructEmail(String subject, String body, UnoUser user) {
        final SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(body);
        email.setTo(user.getEmail());
        email.setFrom(env.getProperty("support.email"));
        return email;
    }

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
