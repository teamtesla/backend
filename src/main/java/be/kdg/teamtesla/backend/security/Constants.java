package be.kdg.teamtesla.backend.security;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static final String SECRET = "SecretKeyToGenJWTs25454615677864546464454644456456446876431resqfd4556123454545345fsdg46845645gdfs4g5sdg45665456456123";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String SIGN_UP_URL = "/user/register";
    public static final String FRONTEND_URL = "http://uno-stage.surge.sh/";
    public static List<String> AUTHERIZED_REDIRECT_URIS = new ArrayList<>();
    public Constants() {
        AUTHERIZED_REDIRECT_URIS.add("http://localhost:3000/oauth2/redirect");
        AUTHERIZED_REDIRECT_URIS.add("http://localhost:3000/oauth2/redirect");
    }
}
