package be.kdg.teamtesla.backend.security.model;

import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.statistics.StatisticsUno;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Base64;
import java.util.Set;

@Entity
public class UnoUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long unoUserId;
    private String username;

    private  String password;
    private String email;
    private boolean verified;
    private String provider;
    private String providerId;
    private String uuid;
    @Transient
    private boolean alive;

    @ManyToOne
    private Lobby currentLobby;
    @ManyToOne
    private Game currentGame;

    @ManyToMany
    private Set<UnoUser> friends;


    @ManyToMany(mappedBy = "unoUsers")
    private Set<Lobby> lobbies;

    @OneToMany(mappedBy = "host")
    private Set<Lobby> hostLobbies;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private StatisticsUno statisticsUno;

    public byte[] getImage() {
        return Base64.getDecoder().decode(image.getBytes());
    }

    public String getImage2() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;
    
    public UnoUser() {
        statisticsUno=new StatisticsUno(this);
        this.verified = false;
    }

    public long getId() {
        return unoUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }


    public Set<Lobby> getLobbies() {
        return lobbies;
    }

    public void setLobby(Set<Lobby> lobbies) {
        this.lobbies = lobbies;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Lobby getCurrentLobby() {
        return currentLobby;
    }

    public void setCurrentLobby(Lobby currentLobby) {
        this.currentLobby = currentLobby;
    }

    public void addLobby(Lobby lobby) {this.lobbies.add(lobby);}

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public StatisticsUno getUserStats() {
        return statisticsUno;
    }

    public void setUserStats(StatisticsUno stats) {
        this.statisticsUno=stats;
    }

    @JsonIgnore

    public Set<UnoUser> getFriends() {
        return friends;
    }


    public void setFriends(Set<UnoUser> friends) {
        this.friends = friends;
    }


}
