package be.kdg.teamtesla.backend.security;

import be.kdg.teamtesla.backend.security.error.ResourceNotFoundException;
import be.kdg.teamtesla.backend.security.error.UserAlreadyExistsException;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.model.VerificationToken;
import be.kdg.teamtesla.backend.security.payload.UserDTO;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import be.kdg.teamtesla.backend.security.persistence.VerificationTokenRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.UUID;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";

    private UserRepo userRepo;
    private VerificationTokenRepo verificationTokenRepo;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserDetailServiceImpl(UserRepo userRepo, VerificationTokenRepo verificationTokenRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepo = userRepo;
        this.verificationTokenRepo = verificationTokenRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UnoUser unoUser = userRepo.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(email));

        return UserPrincipal.create(unoUser);
    }

    @Transactional
    public UserDetails loadByUserId(Long id) {
        UnoUser unoUser = userRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

        return UserPrincipal.create(unoUser);
    }

    @Transactional
    public UnoUser loadUnoUserByUserId(Long id) {
        UnoUser unoUser = userRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

        return unoUser;
    }

    public UnoUser registerUser(final UserDTO userDTO) {
        if (emailExists(userDTO.getEmail())) {
            throw new UserAlreadyExistsException("Account with email already exists: " + userDTO.getEmail());
        }
        final UnoUser unoUser = new UnoUser();
        unoUser.setUsername(userDTO.getUsername());
        unoUser.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        unoUser.setEmail(userDTO.getEmail());

        return userRepo.save(unoUser);
    }

    private boolean emailExists(final String email) {
        return userRepo.findByEmail(email) != null;
    }

    public void createVerificationTokenForUser(final UnoUser user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        verificationTokenRepo.save(myToken);
    }

    public VerificationToken generateNewVerificationToken(final String existingVerificationToken) {
        VerificationToken vToken = verificationTokenRepo.findByToken(existingVerificationToken);
        vToken.updateToken(UUID.randomUUID().toString());
        vToken = verificationTokenRepo.save(vToken);
        return vToken;
    }

    public UnoUser getUser(final String verificationToken) {
        final VerificationToken token = verificationTokenRepo.findByToken(verificationToken);
        if (token != null) {
            return token.getUser();
        }
        return null;
    }

    public String validateVerificationToken(String token) {
        final VerificationToken verificationToken = verificationTokenRepo.findByToken(token);
        if (verificationToken == null) {
            return TOKEN_INVALID;
        }

        final UnoUser user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate()
                .getTime()
                - cal.getTime()
                .getTime()) <= 0) {
            verificationTokenRepo.delete(verificationToken);
            return TOKEN_EXPIRED;
        }

        user.setVerified(true);
        // tokenRepository.delete(verificationToken);
        userRepo.save(user);
        return TOKEN_VALID;
    }
}
