package be.kdg.teamtesla.backend.security;

import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UnoUserService {
    private UserRepo userRepo;
    public UnoUserService(UserRepo userRepo){
        this.userRepo = userRepo;
    }
    public void savePicture(String imageData, long userId) {
        //Connection conn=dbConnect("jdbc:sqlserver://teslaserver.database.windows.net:1433;database=TeslaUno;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;","teslaserver","TeamTesla2");
        //insertImage(conn, imageData.toString(), userId);
        //getImageData(conn);
        UnoUser unoUser =userRepo.getOne(userId);
        unoUser.setImage(imageData);
        userRepo.save(unoUser);
    }

  /*  public void getImageData(Connection conn) {
        byte[] fileBytes;
        String query;
        try {
            query = "select binarystream from ImageTable where id in(select max(id) from ImageTable)";
            Statement state = conn.createStatement();
            ResultSet rs = state.executeQuery(query);
            if (rs.next()) {
                fileBytes = rs.getBytes(1);
                OutputStream targetFile = new FileOutputStream("d:// camImg.JPG");
                targetFile.write(fileBytes);
                targetFile.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void insertImage(Connection conn, String img,long id) {
        String query;
        PreparedStatement pstmt;
        try {
            query = ("insert into ImageTable VALUES(?,?)");
            pstmt = conn.prepareStatement(query);
            pstmt.setLong(2, id);
            pstmt.setString(1, img);
            pstmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Connection dbConnect(String db_connect_string, String db_userid, String db_password) {
        try {
            Connection conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCurrentLobbyOfUser(long userId, Lobby lobby){
        UnoUser unoUser = userRepo.getOne(userId);
        unoUser.setCurrentLobby(lobby);
        userRepo.save(unoUser);
    }
    public void setCurrentGameOfUser(long userId, Game game) {
        UnoUser unoUser = userRepo.getOne(userId);
        unoUser.setCurrentGame(game);
        userRepo.save(unoUser);
    }
    public Optional<UnoUser> findByUserName(String username){
        return userRepo.findByUsername(username);
    }
    public Optional<UnoUser> findByUserEmail(String mail){
        return userRepo.findByEmail(mail);
    }

    public void saveUser(UnoUser user){
        userRepo.save(user);
    }
    public UnoUser findById(long id){
        return userRepo.findById(id);
    }
    public List<UnoUser> findAll(){
        return  userRepo.findAll();
    }
}
