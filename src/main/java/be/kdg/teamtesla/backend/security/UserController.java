package be.kdg.teamtesla.backend.security;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.payload.ApiResponse;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

@Controller
@Transactional
@RequestMapping("/userDetails")
public class UserController {
    private WebsocketDispatcher websocketDispatcher;
    private UnoUserService unoUserService;
    private TokenProvider tokenProvider;

    public UserController(WebsocketDispatcher websocketDispatcher, UnoUserService unoUserService, TokenProvider tokenProvider) {
        this.websocketDispatcher = websocketDispatcher;
        this.unoUserService = unoUserService;
        this.tokenProvider = tokenProvider;
        createSubscriptions();
    }

    private void createSubscriptions(){
        websocketDispatcher.SubscribeOnTopic("user/info", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                UnoUser unoUser = wsm.getUser();
                UnoUserDTO unoUserDTO = new ModelMapper().map(unoUser, UnoUserDTO.class);
                websocketDispatcher.SendMessageToUser(unoUser, "user/info", unoUserDTO);
            }
        }, UnoUserDTO.class);
    }



    @PostMapping(value = "/upload")
    public ResponseEntity<?> pictureUpload(@RequestHeader final String Authorization,@RequestParam("file")MultipartFile multipartFile){
        String token = Authorization.replace("Bearer ", "");
        long id = tokenProvider.getUserIdFromToken(token);
        try{
            String byteString= Base64.getEncoder().encodeToString(multipartFile.getBytes());
            unoUserService.savePicture(byteString,id);
        }catch (IOException e){e.printStackTrace();}

        return ResponseEntity.ok(new ApiResponse(true, "Uploaded a profile pic"));
    }


    @GetMapping(value = "/profilePic/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String getProfilepic(@PathVariable("id") long id){
        if(unoUserService.findById(id) != null)
        return "{\"imageData\": \""+unoUserService.findById(id).getImage2()+"\"}";

        return null;
    }

}
