package be.kdg.teamtesla.backend.security.dto;

import be.kdg.teamtesla.backend.lobby.dto.LobbyDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UnoUserDTO {
    private long unoUserId;
    private String username;
    private String email;
    private boolean verified;
    private String provider;
    private String providerId;
    private String uuid;
//    private long currentLobbyId;
    private boolean alive;
    private LobbyDTO currentLobby;


    public long getUnoUserId() {
        return unoUserId;
    }

    public void setUnoUserId(long unoUserId) {
        this.unoUserId = unoUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @JsonIgnore
    public LobbyDTO getCurrentLobby() {
        return currentLobby;
    }

    @JsonIgnore
    public void setCurrentLobby(LobbyDTO currentLobby) {
        this.currentLobby = currentLobby;
    }
}
