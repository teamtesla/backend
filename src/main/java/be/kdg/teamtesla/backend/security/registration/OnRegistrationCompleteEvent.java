package be.kdg.teamtesla.backend.security.registration;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

@SuppressWarnings("serial")
public class OnRegistrationCompleteEvent extends ApplicationEvent {

    private final String appUrl;
    private final Locale locale;
    private final UnoUser user;

    public OnRegistrationCompleteEvent(final UnoUser user, final Locale locale, final String appUrl) {
        super(user);
        this.user = user;
        this.locale = locale;
        this.appUrl = appUrl;
    }

    //

    public String getAppUrl() {
        return appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public UnoUser getUser() {
        return user;
    }
}
