package be.kdg.teamtesla.backend.security.error;

public final class InvalidVerificationTokenException extends RuntimeException{
    public InvalidVerificationTokenException() {
    }

    public InvalidVerificationTokenException(String message) {
        super(message);
    }

    public InvalidVerificationTokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidVerificationTokenException(Throwable cause) {
        super(cause);
    }
}
