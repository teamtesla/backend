package be.kdg.teamtesla.backend.security.persistence;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<UnoUser, Long> {
    Optional<UnoUser> findByUsername(String username);
    Optional<UnoUser> findByEmail(String email);
    UnoUser findById(long id);
    Boolean existsByEmail(String email);
}
