package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;

import java.util.List;

public class InvitationDTO {
    private long lobby;
    private List<String> receivernames;
    private UnoUserDTO sender;
    private String message;
    private long invitationId;

    public InvitationDTO() {

    }

    public long getLobby() {
        return lobby;
    }
    public void setLobby(long lobby) {
        this.lobby = lobby;
    }
    public List<String> getReceivernames() {
        return receivernames;
    }
    public void setReceivernames(List<String> receivernames) {
        this.receivernames = receivernames;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public UnoUserDTO getSender() {
        return sender;
    }

    public void setSender(UnoUserDTO sender) {
        this.sender = sender;
    }

    public long getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(long invitationId) {
        this.invitationId = invitationId;
    }
}
