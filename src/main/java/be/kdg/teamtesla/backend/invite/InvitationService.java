package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InvitationService {
    private InvitationRepo invitationRepo;

    public InvitationService(InvitationRepo invitationRepo) {
        this.invitationRepo = invitationRepo;
    }

    public Invitation createInvitation(Invitation invitation){
        return invitationRepo.save(invitation);

    }
    public void removeAllInvites(){
        invitationRepo.deleteAll();
    }
    public int getInviteCount(){
        return invitationRepo.findAll().size();
    }
    public List<Invitation> getInvitationsFromUser(UnoUser user){
        return invitationRepo.getAllByReceiver(user);
    }
    @Transactional
    void removeInvitation(long id){
        invitationRepo.deleteByInvitationId(id);
    }
}
