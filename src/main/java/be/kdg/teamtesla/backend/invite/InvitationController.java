package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
public class InvitationController {
    private WebsocketDispatcher websocketDispatcher;
    private InvitationService invitationService;
    private UnoUserService unoUserService;
    private LobbyService lobbyService;

    public InvitationController(WebsocketDispatcher websocketDispatcher, InvitationService invitationService, UnoUserService unoUserService, LobbyService lobbyService) {
        this.websocketDispatcher = websocketDispatcher;
        this.invitationService = invitationService;
        this.unoUserService = unoUserService;
        this.lobbyService = lobbyService;
        createSubscriptions();
    }

    private void createSubscriptions() {
        //process new lobby invite
        websocketDispatcher.SubscribeOnTopic("invitation/send", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if (wsm.getUser() != null) {
                    InvitationDTO invitationDTO = (InvitationDTO) wsm.getObject();
                    for (int i = 0; i < invitationDTO.getReceivernames().size(); i++) {
                        Optional<UnoUser> unoUser = unoUserService.findByUserEmail(invitationDTO.getReceivernames().get(i));
                        if(!unoUser.isPresent()) unoUser = unoUserService.findByUserName(invitationDTO.getReceivernames().get(i));
                        //check if invited user exists && check if invited user != sender
                        if (unoUser.isPresent() && unoUser.get() != wsm.getUser()) {
                            Lobby lobby = lobbyService.getLobbyById(invitationDTO.getLobby());
                            //check if invited user is not in lobby already
                            if(!lobby.getUnoUsers().contains(unoUser.get())) {
                                Invitation invitation = new Invitation(lobby, unoUser.get(), wsm.getUser());
                                invitationDTO.setInvitationId(invitationService.createInvitation(invitation).getInvitationId());
                                invitationDTO.setSender(new ModelMapper().map(wsm.getUser(), UnoUserDTO.class));
                                websocketDispatcher.SendMessageToUser(unoUser.get(), "invitation/new", invitationDTO);
                            }
                        }
                    }
                }
            }
        }, InvitationDTO.class);

        //send all invitations from single user to those who ask to receive
        websocketDispatcher.SubscribeOnTopic("invitation/list", new ISocketListener() {
            @Override
            @Transactional
            public void notify(WebSocketMessage wsm) {
                List<Invitation> invitationList = invitationService.getInvitationsFromUser(wsm.getUser());
                List<InvitationDTO> invitationDTOList;

                Type listType = new TypeToken< List<InvitationDTO>>() {}.getType();
                invitationDTOList = new ModelMapper().map(invitationList,listType);

                websocketDispatcher.SendMessageToUser(wsm.getUser(),"invitation/list",invitationDTOList);
            }
        }, String.class);

        //accept lobby invite
        websocketDispatcher.SubscribeOnTopic("invitation/remove", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {

                //delete notification
                InvitationDTO invitationDTO = (InvitationDTO) wsm.getObject();
                invitationService.removeInvitation(invitationDTO.getInvitationId());


                //send new notification list
                List<Invitation> invitationList = invitationService.getInvitationsFromUser(wsm.getUser());
                List<InvitationDTO> invitationDTOList;

                Type listType = new TypeToken< List<InvitationDTO>>() {}.getType();
                invitationDTOList = new ModelMapper().map(invitationList,listType);

                websocketDispatcher.SendMessageToUser(wsm.getUser(),"invitation/list",invitationDTOList);
            }
        },InvitationDTO.class);
    }
}
