package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;

import javax.persistence.*;

@Entity
public class Invitation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long invitationId;

    @ManyToOne
    private Lobby lobby;

    @ManyToOne
    private UnoUser receiver;

    @ManyToOne
    private UnoUser sender;

    private String message;

    public Invitation(Lobby lobby, UnoUser receiver, UnoUser sender) {
        this.lobby = lobby;
        this.receiver = receiver;
        this.sender = sender;
        this.message = sender.getEmail() + " invited you in his lobby";
    }

    public Invitation() {
    }

    public long getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(long invitationId) {
        this.invitationId = invitationId;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public UnoUser getReceiver() {
        return receiver;
    }

    public void setReceiver(UnoUser receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public UnoUser getSender() {
        return sender;
    }

    public void setSender(UnoUser sender) {
        this.sender = sender;
    }
}
