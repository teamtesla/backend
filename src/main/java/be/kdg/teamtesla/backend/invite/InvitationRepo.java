package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface InvitationRepo extends JpaRepository<Invitation, Long> {
    List<Invitation> getAllByReceiver(UnoUser user);
    @Transactional
    @Modifying
    void deleteByInvitationId(long id);
}
