package be.kdg.teamtesla.backend.lobby.model;

import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Lobby {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long lobbyId;
    private String name;




    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "host_id", nullable = false)
    private UnoUser host;

//, CascadeType.PERSIST
    @ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST },fetch = FetchType.EAGER)
    @JoinTable(
            name = "UnoUser_Lobby",
            joinColumns = { @JoinColumn(name = "lobby_id") },
            inverseJoinColumns = { @JoinColumn(name = "uno_user_id") }
    )
    private Set<UnoUser> unoUsers;
    //, CascadeType.PERSIST
    @OneToMany(mappedBy = "lobby")
    private Set<Game> games;

    @OneToOne
    private Game currentGame;
    private int playerCount;
    private String uuid;
    private boolean isPrivate;

    public Lobby(String name, UnoUser host, int playerCount) {
        this.name = name;
        this.host = host;
        this.unoUsers = new HashSet<>();
        this.games= new HashSet<>();
        this.playerCount = playerCount;
        this.unoUsers.add(host);
    }

    public Lobby(String name, UnoUser host) {
        this.name = name;
        this.host = host;
        this.unoUsers = new HashSet<>();
        this.games= new HashSet<>();
        this.playerCount = 4;
        this.unoUsers.add(host);
    }

    public Lobby() {
    }

    public void removePlayer(UnoUser unoUser){
        for (UnoUser user : unoUsers) {
            if(unoUser.getId() == user.getId()){
                unoUsers.remove(user);
            }
        }
    }

    public void addPlayer(UnoUser unoUser){
        unoUsers.add(unoUser);
    }

    public long getLobbyId() {
        return lobbyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public UnoUser getHost() {
        return host;
    }

    @JsonIgnore
    public void setHost(UnoUser host) {
        this.host = host;
    }

    @JsonIgnore
    public Set<UnoUser> getUnoUsers() {
        return unoUsers;
    }

    @JsonIgnore
    public void setUnoUsers(Set<UnoUser> unoUsers) {
        this.unoUsers = unoUsers;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public String getUuid() {
        return uuid;
    }

    @JsonIgnore
    public Set<Game> getGames() {
        return games;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @Override
    public String toString() {
        return "{\"lobby\":{" +
                "\"name\":" + "\"" + name + "\""+
                "\"host\":" + "\"" +host + "\""+
                "\"playerCount\":" + "\"" + playerCount + "\""+
                "\"players\":" + "\"" + unoUsers + "\""+
                "}}";
    }

    public void addGame(Game game) {
        this.games.add(game);
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }
}
