package be.kdg.teamtesla.backend.lobby;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@Service
@Transactional
public class LobbyService {
    private final LobbyRepo lobbyRepo;
    private final UserRepo userRepo;

    public LobbyService(LobbyRepo lobbyRepo, UserRepo userRepo ) {
        this.lobbyRepo = lobbyRepo;
        this.userRepo = userRepo;
    }

    public Lobby createLobby(Lobby lobby){
        return lobbyRepo.save(lobby);
    }

    public Lobby createLobby(String lobbyName, long userid){
        UnoUser unoUser = userRepo.findById(userid);
        Lobby lobby = new Lobby(lobbyName, unoUser, 4);
        lobby.setUuid(UUID.randomUUID().toString());
        return lobbyRepo.save(lobby);
    }

    public List<Lobby> getLobbies(){ return lobbyRepo.findByIsPrivate(false);}

    public List<Lobby> getLobbyByUser(long userid){
        UnoUser unoUser = userRepo.findById(userid);
        return lobbyRepo.findByUnoUsers(unoUser);
    }
    public Lobby getLobbyById(long lobbyId){ return lobbyRepo.findByLobbyId(lobbyId);}

    public boolean isPlayerInLobby(long playerId, long lobbyId){
        final AtomicReference<Boolean> returnBool = new AtomicReference<>();
        returnBool.set(false);
        Lobby l = getLobbyById(lobbyId);
        l.getUnoUsers().forEach((unoUser -> {
            if(unoUser.getId() == playerId) returnBool.set(true);
        }));
        return returnBool.get();
    }
    public void removeLobbies() {lobbyRepo.deleteAll();}
//    public void addSpelerToLobby(long id, Player player){
//        Optional<Lobby> lobby = lobbyRepo.findById(id);
//        lobby.get().addPlayer(player);
//    }
    public void update(Lobby l){
        lobbyRepo.save(l);
    }

    public Lobby addUser(long lobbyId, long userId) {
        Lobby lobby = lobbyRepo.findByLobbyId(lobbyId);
        UnoUser user = userRepo.findById(userId);
        lobby.addPlayer(user);
        return lobbyRepo.save(lobby);
    }

    public int getCurrentUserAmount(long lobbyId) {
        return lobbyRepo.findByLobbyId(lobbyId).getUnoUsers().size();
    }

    public int getCurrentGameAmount(long lobbyId) {
        return lobbyRepo.findByLobbyId(lobbyId).getGames().size();
    }

    public void removeLobby(Lobby lobby){
        lobbyRepo.delete(lobby);
    }
    public void leaveLobby(long ld,UnoUser unoUser){
        Lobby lobby = lobbyRepo.findByLobbyIdAndUnoUsers(ld, unoUser);
        lobby.removePlayer(unoUser);
    }
}
