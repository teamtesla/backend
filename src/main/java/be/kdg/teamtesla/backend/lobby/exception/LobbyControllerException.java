package be.kdg.teamtesla.backend.lobby.exception;

public class LobbyControllerException extends RuntimeException {
    public LobbyControllerException(String message) {
        super(message);
    }

    public LobbyControllerException(String message, Throwable cause) {
        super(message, cause);
    }
}
