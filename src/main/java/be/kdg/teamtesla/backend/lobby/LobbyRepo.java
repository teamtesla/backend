package be.kdg.teamtesla.backend.lobby;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LobbyRepo extends JpaRepository<Lobby, Long> {
    Lobby findByName(String name);
    Lobby findByLobbyId(long lobbyId);
    List<Lobby> findByUnoUsers(UnoUser unoUser);
    Lobby findByLobbyIdAndUnoUsers(long lobbyid, UnoUser unoUser);
    List<Lobby> findByIsPrivate(boolean isPrivate);
}
