package be.kdg.teamtesla.backend.lobby.dto;

import javax.validation.constraints.Size;

public class LobbyCreateDTO {
    @Size(max = 50)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
