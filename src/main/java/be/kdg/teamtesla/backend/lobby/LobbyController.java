package be.kdg.teamtesla.backend.lobby;

import be.kdg.teamtesla.backend.game.GameManager;
import be.kdg.teamtesla.backend.lobby.dto.LobbyChangeDTO;
import be.kdg.teamtesla.backend.lobby.dto.LobbyCreateDTO;
import be.kdg.teamtesla.backend.lobby.dto.LobbyDTO;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.userState.UserStateManager;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Controller
@Transactional
public class LobbyController {
    private UnoUserService userService;
    private LobbyService lobbyService;
    private GameManager gameManager;
    private WebsocketDispatcher websocketDispatcher;
    private UserStateManager userStateManager;


    public LobbyController(LobbyService lobbyService, GameManager gameController, WebsocketDispatcher websocketDispatcher, UnoUserService unoUserService, UserStateManager userStateManager) {
        this.lobbyService = lobbyService;
        this.gameManager=gameController;
        this.websocketDispatcher = websocketDispatcher;
        this.userService = unoUserService;
        this.userStateManager = userStateManager;
        createSubscriptions();
    }

    //CREATE ENDPOINTS
    void createSubscriptions(){
        //Called to create a new lobby and also change to that lobby
        websocketDispatcher.SubscribeOnTopic("lobby/create", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {

                try {
                    if(wsm.getUser() != null){
                        LobbyCreateDTO lobbyCreateDTO = (LobbyCreateDTO)wsm.getObject();
                        Lobby lobby = createLobby(lobbyCreateDTO.getName(), wsm.getUser().getId());
                        LobbyDTO lobbyDTO = new ModelMapper().map(lobby, LobbyDTO.class);
                        lobbyDTO.setIsHost(true);
                        userService.setCurrentLobbyOfUser(wsm.getUser().getId(),lobby);
                        websocketDispatcher.SendMessageToUser(wsm.getUser(),"lobby/current",lobbyDTO);

                        //SEND NEW LIST OF LOBBIES TO ALL USERS
                        Type listType = new TypeToken<List<LobbyDTO>>() {}.getType();
                        List<LobbyDTO> lobbies = new ModelMapper().map(getLobby(),listType);
                        websocketDispatcher.SendMessageToAllUsers("lobby/list",lobbies);
                    }

                    keepAlive(wsm.getUser());
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }

            }
        }, LobbyCreateDTO.class);

        //Called to get a single lobby
        websocketDispatcher.SubscribeOnTopic("lobby/get", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {

                try {
                    Lobby lobby = wsm.getUser().getCurrentLobby();
                    if(lobby != null) {
                        LobbyDTO lobbyDTO = new ModelMapper().map(lobby, LobbyDTO.class);
                        if(lobby.getHost().getId() == wsm.getUser().getId()) lobbyDTO.setIsHost(true);
                        websocketDispatcher.SendMessageToUser(wsm.getUser(), "lobby/current", lobbyDTO);
                    }
                    keepAlive(wsm.getUser());
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }

            }
        },String.class);

        websocketDispatcher.SubscribeOnTopic("lobby/personal", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if(wsm.getUser() != null){
                    List<Lobby> lobbies = lobbyService.getLobbyByUser(wsm.getUser().getId());
                    Type listType = new TypeToken<List<LobbyDTO>>() {}.getType();
                    List<LobbyDTO> lobbyDTOS = new ModelMapper().map(lobbies,listType);
                    websocketDispatcher.SendMessageToUser(wsm.getUser(), "lobby/personal", lobbyDTOS);
                }
            }
        }, String.class);

        //Called to receive a new list of lobbies
        websocketDispatcher.SubscribeOnTopic("lobby/list", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                try {
                    if(wsm.getUser() != null){
                        Type listType = new TypeToken<List<LobbyDTO>>() {}.getType();
                        List<LobbyDTO> lobbies = new ModelMapper().map(getLobby(),listType);
                        websocketDispatcher.SendMessageToUser(wsm.getUser(), "lobby/list",lobbies);
                    }
                    keepAlive(wsm.getUser());
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }
            }
        }, String.class);

        //Called when users want to change lobby
        websocketDispatcher.SubscribeOnTopic("lobby/change", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                try {
                    //Get Lobby user wants to switch to and put it in dto object
                    LobbyChangeDTO lobbyChangeDTO = (LobbyChangeDTO)wsm.getObject();
                    long id = lobbyChangeDTO.getId();
                    Lobby lobby = lobbyService.getLobbyById(id);
                    LobbyDTO lobbyDTO = new ModelMapper().map(lobby, LobbyDTO.class);

                    //If player is already in lobby do nothing
                    if(lobbyService.isPlayerInLobby(wsm.getUser().getId(),lobby.getLobbyId())){
                        userService.setCurrentLobbyOfUser(wsm.getUser().getId(), lobby);
                    }
                    else
                    {
                        //If lobby is full do nothing
                        if(lobby.getPlayerCount() <= lobby.getUnoUsers().size()){
                            //DO NOTHING
                        }
                        //Else put user in lobby
                        else{
                            lobby.getUnoUsers().add(wsm.getUser());
                            lobbyService.update(lobby);
                            lobbyDTO = new ModelMapper().map(lobby, LobbyDTO.class);
                            userService.setCurrentLobbyOfUser(wsm.getUser().getId(),lobby);
                        }
                    }
                    AtomicReference<LobbyDTO> atomicLobbyDTO = new AtomicReference<LobbyDTO>();
                    atomicLobbyDTO.set(new ModelMapper().map(lobby, LobbyDTO.class));

                    lobby.getUnoUsers().forEach((unoUser -> {

                        //Check if other users are hosts
                        LobbyDTO ldto = atomicLobbyDTO.get();
                        if (ldto.getHost().getUnoUserId() != unoUser.getId()) ldto.setIsHost(false);
                        else ldto.setIsHost(true);
                        atomicLobbyDTO.set(ldto);

                        //Send update to other users
                        if(unoUser.getId() != wsm.getUser().getId())
                        websocketDispatcher.SendMessageToUser(unoUser,"lobby/updateCurrent",atomicLobbyDTO.get());
                    }));
                    //Check if current user is host
                    LobbyDTO ldto = atomicLobbyDTO.get();
                    if(wsm.getUser().getId() == ldto.getHost().getUnoUserId())ldto.setIsHost(true);
                    else ldto.setIsHost(false);
                    //Send lobby switch to current user
                    websocketDispatcher.SendMessageToUser(wsm.getUser(),"lobby/current",atomicLobbyDTO.get());

                    keepAlive(wsm.getUser());
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }
            }
        }, LobbyChangeDTO.class);

        //Called when a user changes a setting from a lobby or joins a lobby
        websocketDispatcher.SubscribeOnTopic("lobby/update", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                try {
                    LobbyDTO lobbyDTO = (LobbyDTO)wsm.getObject();
                    Lobby lobby = lobbyService.getLobbyById(lobbyDTO.getLobbyId());

                    if(wsm.getUser().getId() == lobby.getHost().getId()){
                        lobby.setName(lobbyDTO.getName());
                        lobby.setPlayerCount(lobbyDTO.getPlayerCount());
                        lobby.setIsPrivate(lobbyDTO.getIsPrivate());
                        lobbyService.update(lobby);

                        AtomicReference<LobbyDTO> atomicLobbyDTO = new AtomicReference<LobbyDTO>();
                        atomicLobbyDTO.set(new ModelMapper().map(lobby, LobbyDTO.class));

                        lobby.getUnoUsers().forEach((unoUser -> {
                            if(unoUser.getId() == atomicLobbyDTO.get().getHost().getUnoUserId()){
                                LobbyDTO ldto = atomicLobbyDTO.get();
                                ldto.setIsHost(true);
                                atomicLobbyDTO.set(ldto);
                            }
                            else{
                                LobbyDTO ldto = atomicLobbyDTO.get();
                                ldto.setIsHost(false);
                                atomicLobbyDTO.set(ldto);
                            }
                            websocketDispatcher.SendMessageToUser(unoUser,"lobby/updateCurrent",atomicLobbyDTO.get());
                        }));

                        //SEND NEW LIST OF LOBBIES TO ALL USERS
                        Type listType = new TypeToken<List<LobbyDTO>>() {}.getType();
                        List<LobbyDTO> lobbies = new ModelMapper().map(getLobby(),listType);
                        websocketDispatcher.SendMessageToAllUsers("lobby/list",lobbies);
                    }
                    keepAlive(wsm.getUser());
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }
            }
        }, LobbyDTO.class);

        websocketDispatcher.SubscribeOnTopic("lobby/remove", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                try {
                    if(wsm.getUser() != null){
                        LobbyChangeDTO lobbyChangeDTO = (LobbyChangeDTO) wsm.getObject();
                        long lobbyid = lobbyChangeDTO.getId();
                        Lobby lobby = lobbyService.getLobbyById(lobbyid);
                        lobbyService.removeLobby(lobby);
                        Type listType = new TypeToken<List<LobbyDTO>>() {}.getType();
                        List<LobbyDTO> lobbies = new ModelMapper().map(getLobby(),listType);


                        websocketDispatcher.SendMessageToAllUsers("lobby/list",lobbies);
                        LobbyDTO lobbyDTO = new LobbyDTO();
                        lobbyDTO.setDeleted(true);
                        lobby.getUnoUsers().forEach(u ->{
                            websocketDispatcher.SendMessageToUser(u,"lobby/current",lobbyDTO);
                        });
                    }
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }
            }
        }, LobbyChangeDTO.class);

        websocketDispatcher.SubscribeOnTopic("lobby/leave", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                try {
                    if(wsm.getUser() != null){
                        LobbyChangeDTO lobbydto = (LobbyChangeDTO) wsm.getObject();
                        long lobbyid = lobbydto.getId();

                        lobbyService.leaveLobby(lobbyid, wsm.getUser());
                    }
                } catch (Exception e) {
                    System.out.println("[LOBBY CONTROLLER] error");
                    e.printStackTrace();
                }
            }
        }, LobbyChangeDTO.class);
    }
    private void keepAlive(UnoUser user){
        userStateManager.addOnlineUser(user);
    }


    //FUNCTIONALITYvoor commu
    private List<Lobby> getLobby(){
        return lobbyService.getLobbies();
    }
    public Lobby createLobby(String object, long userid){
        return lobbyService.createLobby(object, userid);
    }

    public Lobby addUser(long lobbyId, long userId) {
        return lobbyService.addUser(lobbyId,userId);
    }
}
