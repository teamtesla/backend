package be.kdg.teamtesla.backend.lobby.exception;

public class LobbyServiceException extends RuntimeException {
    public LobbyServiceException(String message) {
        super(message);
    }

    public LobbyServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
