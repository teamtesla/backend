package be.kdg.teamtesla.backend.lobby.dto;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.Set;

public class LobbyDTO {
    private long lobbyId;
    @Size(max = 50)
    private String name;
    private UnoUserDTO host;
    private Set<UnoUserDTO> unoUsers;
    //private Set<Game> games;
    @Max(10)
    private int playerCount;
    private String uuid;
    private boolean isHost = false;
    private boolean deleted = false;
    private boolean isPrivate;

    public long getLobbyId() {
        return lobbyId;
    }
    public void setLobbyId(long lobbyId) {
        this.lobbyId = lobbyId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public UnoUserDTO getHost() {
        return host;
    }

    public void setHost(UnoUserDTO host) {
        this.host = host;
    }

    public Set<UnoUserDTO> getUnoUsers() {
        return unoUsers;
    }

    public void setUnoUsers(Set<UnoUserDTO> unoUsers) {
        this.unoUsers = unoUsers;
    }

    /*
    public Set<Game> getGames() {
        return games;
    }
    public void setGames(Set<Game> games) {
        this.games = games;
    }
*/
    public int getPlayerCount() {
        return playerCount;
    }
    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }
    public String getUuid() {
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean getIsHost() {
        return isHost;
    }

    public void setIsHost(boolean host) {
        isHost = host;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
