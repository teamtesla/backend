package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.card.Card;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "history_player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long historyPlayerId;
    @ManyToMany()
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinTable(
            name = "History_Card",
            joinColumns = { @JoinColumn(name = "history_player_id") },
            inverseJoinColumns = { @JoinColumn(name = "card_id")}
    )
    private Set<Card> hand;
    private String username;

    public Player() {
    }

    public Player(Set<Card> hand) {
        this.hand = hand;
    }

    public Player(Set<Card> hand, String username) {
        this.hand = hand;
        this.username = username;
    }

    public long getHistoryPlayerId() {
        return historyPlayerId;
    }

    public Set<Card> getHand() {
        return hand;
    }

    public void setHand(Set<Card> hand) {
        this.hand = hand;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
