package be.kdg.teamtesla.backend.history.dto;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.history.History;
import be.kdg.teamtesla.backend.player.dto.PlayerDTO;

import java.util.ArrayList;
import java.util.List;

public class HistoryDTO {
    private Card card;
    private List<PlayerDTO> players;
    private List<TurnDTO> turns;

    public HistoryDTO() {
    }

    public HistoryDTO(History history, int index) {
        card = history.getStartingCard();
        players = new ArrayList<>();
        long id = history.getPlayers().get(index).getHistoryPlayerId();
        history.getPlayers().forEach(player -> {
            PlayerDTO playerDTO = new PlayerDTO();
            if (player.getHistoryPlayerId() == id) playerDTO.setCards(new ArrayList<>(player.getHand()));
            playerDTO.setUserId(player.getHistoryPlayerId());
            playerDTO.setAmount(player.getHand().size());
            playerDTO.setUsername(player.getUsername());
            playerDTO.setTurn(false);
            players.add(playerDTO);
        });
        turns = new ArrayList<>();
        history.getTurns().forEach(turn -> turns.add(new TurnDTO(turn, id)));
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public List<PlayerDTO> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDTO> players) {
        this.players = players;
    }

    public List<TurnDTO> getTurns() {
        return turns;
    }

    public void setTurns(List<TurnDTO> turns) {
        this.turns = turns;
    }
}
