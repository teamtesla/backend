package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.card.Card;

import javax.persistence.*;

@Entity
public class Turn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long turnId;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "card_id", nullable = false)
    private Card card;
    @Enumerated(EnumType.ORDINAL)
    private Action action;
    private long player;

    public Turn() {
    }

    public long getTurnId() {
        return turnId;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public long getPlayer() {
        return player;
    }

    public void setPlayer(long player) {
        this.player = player;
    }
}
