package be.kdg.teamtesla.backend.history.dto;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.history.Action;
import be.kdg.teamtesla.backend.history.Turn;

public class TurnDTO {
    private Card card;
    private Action action;
    private long player;

    public TurnDTO() {
    }

    public TurnDTO(Turn turn, long id) {
        this.action = turn.getAction();
        this.player = turn.getPlayer();
        if (this.player == id || this.action.equals(Action.PLAY)) {
            card = turn.getCard();
        }
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public long getPlayer() {
        return player;
    }

    public void setPlayer(long player) {
        this.player = player;
    }
}
