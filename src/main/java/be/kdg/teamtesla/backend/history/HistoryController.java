package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.game.GameService;
import be.kdg.teamtesla.backend.history.dto.HistoryDTO;
import be.kdg.teamtesla.backend.history.exception.UserNotInGameException;
import be.kdg.teamtesla.backend.security.TokenProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Transactional
public class HistoryController {
    private GameService gameService;
    private TokenProvider tokenProvider;

    public HistoryController(GameService gameService, TokenProvider tokenProvider) {
        this.gameService = gameService;
        this.tokenProvider = tokenProvider;
    }

    @GetMapping(value = "/history")
    public ResponseEntity<HistoryDTO> getHistory(@RequestHeader final String Authorization, @RequestParam("game") long gameId) {
        String token = Authorization.replace("Bearer ", "");
        long id = tokenProvider.getUserIdFromToken(token);
        Game game = gameService.getGameById(gameId);
        try {
            if (game != null) {
                int turn = game.getPlayers().indexOf(game.getPlayers().stream().filter(player -> player.getUnoUser().getId() == id).findAny().orElseThrow(UserNotInGameException::new));
                HistoryDTO historyDTO = new HistoryDTO(game.getHistory(), turn);
                return ResponseEntity.ok(historyDTO);
            }
        } catch (UserNotInGameException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.notFound().build();
    }
}
