package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.card.Card;

import javax.persistence.*;
import java.util.List;

@Entity
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long historyId;
    @ManyToOne()


    @JoinColumn(name = "card_id", nullable = false)
    private Card startingCard;
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "history_id")
    private List<Player> players;
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "history_id")
    private List<Turn> turns;

    public History() {
    }

    public long getHistoryId() {
        return historyId;
    }

    public Card getStartingCard() {
        return startingCard;
    }

    public void setStartingCard(Card startingCard) {
        this.startingCard = startingCard;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public void addTurn(Turn turn) {
        this.turns.add(turn);
    }
}
