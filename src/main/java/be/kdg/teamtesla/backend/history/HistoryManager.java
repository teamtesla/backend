package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.game.Game;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class HistoryManager {
    public History createHistory(Game game) {
        History history = new History();
        history.setStartingCard(game.getTopDiscardCard());
        history.setTurns(new ArrayList<>());
        history.setPlayers(new ArrayList<>());
        game.getPlayers().forEach(player -> {
            if (player.getUnoUser() != null) {
                history.addPlayer(new Player(player.getHand(), player.getUnoUser().getUsername()));
            }
            else history.addPlayer(new Player(player.getHand(), "Computer"));
        });
        return history;
    }

    public Turn createTurn(Card card, int turn, boolean play, History history) {
        Turn turnCreate = new Turn();
        if (play) turnCreate.setAction(Action.PLAY);
        else turnCreate.setAction(Action.DRAW);
        turnCreate.setCard(card);
        turnCreate.setPlayer(history.getPlayers().get(turn).getHistoryPlayerId());
        return turnCreate;
    }
}
