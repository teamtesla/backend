package be.kdg.teamtesla.backend.card;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cardId;
    @Enumerated(EnumType.ORDINAL)
    private Color color;
    @Enumerated(EnumType.ORDINAL)
    private Value value;
    /*@ManyToMany(mappedBy = "deck")
    private Set<Game> gamedecks;
    @ManyToMany(mappedBy = "discard")
    private Set<Game> gamediscards;
    @ManyToMany(mappedBy = "hand")
    private Set<Player> playerhands;*/

    public Card() {
    }

    public Card(Color color, Value value) {
        this.color = color;
        this.value = value;
    }

    public Color getColor() {
        return color;
    }

    public Value getValue() {
        return value;
    }

    public long getCardId() {
        return cardId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return getCardId() == card.getCardId() &&
                getColor() == card.getColor() &&
                getValue() == card.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCardId(), getColor(), getValue());
    }
}
