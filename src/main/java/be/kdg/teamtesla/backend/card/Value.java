package be.kdg.teamtesla.backend.card;

public enum Value {
    ZERO(0),ONE(1),TWO(2),THREE(3),FOUR(4),FIVE(5),SIX(6),SEVEN(7),EIGHT(8),NINE(9),REVERSE(20),SKIP(20),TAKETWO(20),CHANGE(50),TAKEFOUR(50);

    private final int score;

    Value(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}
