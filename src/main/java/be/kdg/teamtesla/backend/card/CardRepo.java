package be.kdg.teamtesla.backend.card;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepo  extends JpaRepository<Card,Long> {
    Card getCardByCardId(Long cardId);
}
