package be.kdg.teamtesla.backend.card;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CardService {
    CardRepo cardRepo;

    public CardService(CardRepo cardRepo) {
        this.cardRepo = cardRepo;
    }
    

    public List<Card> getAllCards(){
        return cardRepo.findAll();
    }

    public Card getCardById(Long cardId){
        return cardRepo.getCardByCardId(cardId);
    }
}
