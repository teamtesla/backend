package be.kdg.teamtesla.backend.player;

import be.kdg.teamtesla.backend.game.GameService;
import be.kdg.teamtesla.backend.security.UserDetailServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PlayerService {
    private final PlayerRepo playerRepo;
    private final GameService gameService;
    private final UserDetailServiceImpl userDetailService;

    public PlayerService(PlayerRepo playerRepo, GameService gameService, UserDetailServiceImpl userDetailService) {
        this.playerRepo = playerRepo;
        this.gameService = gameService;
        this.userDetailService = userDetailService;
    }

    public Player createPlayer(Player player){
        return playerRepo.save(player);
    }

    public Player getPlayerByGameAndUnoUser(String uuid, long id) {
        return playerRepo.findPlayerByGameAndUnoUser(gameService.getGameByUuid(uuid), userDetailService.loadUnoUserByUserId(id));
    }

    public Player getPlayerByPlayerId(long playerId){
        return playerRepo.findPlayerByPlayerId(playerId);
    }
}
