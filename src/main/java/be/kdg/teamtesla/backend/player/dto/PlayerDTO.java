package be.kdg.teamtesla.backend.player.dto;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.player.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerDTO {
    private long userId;
    private int amount;
    private List<Card> cards;
    private boolean turn;
    private String username;

    public PlayerDTO() {
    }

    public PlayerDTO(int amount, long userId, boolean turn, String username) {
        this.userId = userId;
        this.amount = amount;
        this.turn = turn;
        this.username = username;
    }

    public PlayerDTO(long userId, int amount, List<Card> cards, boolean turn, String username) {
        this.userId = userId;
        this.amount = amount;
        this.cards = cards;
        this.turn = turn;
        this.username = username;
    }

    public PlayerDTO(Player player) {
        setUserId(player.getUnoUser().getId());
        this.amount = player.getHand().size();
        this.cards = new ArrayList<>(player.getHand());
        this.turn = player.isTurn();
        this.username = player.getUnoUser().getUsername();
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
