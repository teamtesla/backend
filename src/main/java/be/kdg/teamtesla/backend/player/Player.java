package be.kdg.teamtesla.backend.player;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long playerId;

    @ManyToOne
    private UnoUser unoUser;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @ManyToMany()
    @JoinTable(
            name = "Player_Card",
            joinColumns = { @JoinColumn(name = "player_id") },
            inverseJoinColumns = { @JoinColumn(name = "card_id")}
    )
    private Set<Card> hand;
    @Nullable
    private boolean turn;

    @Column
    private Boolean computer;


    public Player(UnoUser userId) {
        this.hand = new HashSet<>();
        this.unoUser = userId;
        this.computer = false;
    }

    public Player(UnoUser userId, Game game) {
        this.hand = new HashSet<>();
        this.unoUser = userId;
        this.game = game;
        this.computer = false;
    }

    public Player() {
        this.hand = new HashSet<>();
        this.computer = true;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    @JsonIgnore
    public UnoUser getUnoUser() {
        return unoUser;
    }

    @JsonIgnore
    public Set<Card> getHand() {
        return hand;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean isComputer() {
        return computer;
    }

    public void setComputer(boolean computer) {
        this.computer = computer;
    }
}
