package be.kdg.teamtesla.backend.player;

import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;



@Controller
@Transactional
public class PlayerController {
    private WebsocketDispatcher websocketDispatcher;
    private PlayerService playerService;
    public PlayerController(WebsocketDispatcher websocketDispatcher,PlayerService playerService) {
        this.websocketDispatcher = websocketDispatcher;
        this.playerService=playerService;
        createSubscriptions();
    }

    private void createSubscriptions() {

    }

}
