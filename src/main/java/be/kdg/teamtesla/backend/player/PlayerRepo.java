package be.kdg.teamtesla.backend.player;

import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepo extends JpaRepository<Player, Long> {
    Player findPlayerByGameAndUnoUser(Game game, UnoUser unoUser);
    Player findPlayerByPlayerId(long playerId);
}
