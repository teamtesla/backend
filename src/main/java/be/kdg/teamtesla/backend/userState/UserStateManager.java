package be.kdg.teamtesla.backend.userState;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class UserStateManager {
    private List<UnoUser> connectedUsers = new ArrayList<>();
    private WebsocketDispatcher dispatcher;

    public UserStateManager(WebsocketDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void addOnlineUser(UnoUser user){
        AtomicReference<Boolean> isConnected = new AtomicReference<>();
        isConnected.set(false);
        connectedUsers.forEach(cu -> {if(cu.getId() == user.getId()) isConnected.set(true); });
        if (!isConnected.get()) {
            user.setAlive(true);
            connectedUsers.add(user);
        notifyUserStateChange(user,true)
        ;
        }
    }
    public List<UnoUser> getConnectedUsers(){
        return connectedUsers;
    }
    //TODO: fix and uncomment this job
    //@Scheduled(fixedDelay = 100000)
    public void checkAlive(){
        connectedUsers.forEach(unoUser -> {
            unoUser.setAlive(false);
        });
        connectedUsers.forEach(unoUser -> {
            dispatcher.SendMessageToUser(unoUser,"ping","");
        });

        try {
            Thread.sleep(2000);
        }
        catch (Exception ex){

        }
        List<UnoUser> remove = new ArrayList<>();
        //remove offline users
        connectedUsers.forEach(unoUser -> {
            if(unoUser.isAlive() == false){
                notifyUserStateChange(unoUser,false);
                remove.add(unoUser);
                System.out.println("USER DISCONNECT");
            }
        });
        connectedUsers.removeAll(remove);
    }
    public void pingAlive(UnoUser user){
        connectedUsers.forEach(c -> {if(c.getId() == user.getId()) c.setAlive(true);});
    }

    //Notify people when someone comes online or goes offline
    private void notifyUserStateChange(UnoUser user,boolean connected){
        //Notify all people in the user's lobbies
        user.getLobbies().forEach(lobby -> {
            lobby.getUnoUsers().forEach(unoUser -> {
                UnoUserDTO unoUserDTO = new ModelMapper().map(user,UnoUserDTO.class);
                unoUserDTO.setAlive(connected);
                dispatcher.SendMessageToUser(unoUser, "userStateChange",unoUserDTO);
            });
        });
    }
}
