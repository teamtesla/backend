package be.kdg.teamtesla.backend.userState;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.List;

@Controller
@Transactional
public class UserStateController {
    private UserStateManager userStateManager;
    private WebsocketDispatcher websocketDispatcher;

    public UserStateController(UserStateManager userStateManager, WebsocketDispatcher websocketDispatcher) {
        this.userStateManager = userStateManager;
        this.websocketDispatcher = websocketDispatcher;
        this.createSubscriptions();
    }
    void createSubscriptions(){
        websocketDispatcher.SubscribeOnTopic("user/state/alive", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                userStateManager.pingAlive(wsm.getUser());
            }
        },String.class);

        websocketDispatcher.SubscribeOnTopic("user/state/get", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                Type listType = new TypeToken<List<UnoUserDTO>>() {}.getType();
                List<UnoUserDTO> unousers = new ModelMapper().map(userStateManager.getConnectedUsers(),listType);
                unousers.forEach((unoUserDTO -> unoUserDTO.setAlive(true)));
                websocketDispatcher.SendMessageToUser(wsm.getUser(),"user/state/get",unousers);
            }
        }, String.class);
    }

}
