package be.kdg.teamtesla.backend.webSockets;

import be.kdg.teamtesla.backend.security.model.UnoUser;

public class WebSocketMessage {
    private String topic;
    private Object object;
    private UnoUser user;

    public WebSocketMessage(String topic, Object object, UnoUser user) {
        this.topic = topic;
        this.object = object;
        this.user = user;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public UnoUser getUser() {
        return user;
    }

    public void setUser(UnoUser user) {
        this.user = user;
    }
}
