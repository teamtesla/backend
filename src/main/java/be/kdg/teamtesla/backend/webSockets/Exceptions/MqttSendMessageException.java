package be.kdg.teamtesla.backend.webSockets.Exceptions;

public class MqttSendMessageException extends RuntimeException {
    public MqttSendMessageException(String message) {
        super(message);
    }

    public MqttSendMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}
