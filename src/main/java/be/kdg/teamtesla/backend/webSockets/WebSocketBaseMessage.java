package be.kdg.teamtesla.backend.webSockets;

public class WebSocketBaseMessage {
    private String token;
    private String object;

    public WebSocketBaseMessage(String token, String object) {
        this.token = token;
        this.object = object;
    }

    public WebSocketBaseMessage() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }
}
