package be.kdg.teamtesla.backend.webSockets;

import be.kdg.teamtesla.backend.security.TokenProvider;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import be.kdg.teamtesla.backend.webSockets.Exceptions.DispatcherSendMessageException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class WebsocketDispatcher  implements MqttCallback {


    private WebSocketService webSocketService;
    private TokenProvider tokenProvider;
    private UserRepo userRepo;
    ArrayList<TopicCallback> topicCallbacks = new ArrayList();


    public WebsocketDispatcher(WebSocketService wss, TokenProvider tp, UserRepo ur) {
        this.webSocketService = wss;
        this.tokenProvider = tp;
        this.userRepo = ur;
        webSocketService.setCallback(this);
    }



    public void SubscribeOnTopic(String topic, ISocketListener socketListener, Class c){

        webSocketService.subscribe(topic);
        AtomicBoolean makeNew = new AtomicBoolean(true);
        topicCallbacks.forEach(tc ->{
            if (tc.getTopic().equals(topic)){
                makeNew.set(false);
                tc.getTypeListeners().add(new TypeListener(c,socketListener));
            }
        });
        if (makeNew.get()){
            TopicCallback tc = new TopicCallback(topic);
            tc.getTypeListeners().add(new TypeListener(c, socketListener));
            topicCallbacks.add(tc);
        }
    }

    public void SendMessage(String topic, Object object){
        try{
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(object);
        webSocketService.sendMessage(topic, json);
        }
        catch (Exception ex){
            System.out.println("EXCEPTION: " + ex.getMessage());
            throw new DispatcherSendMessageException(ex.getMessage());
        }
    }
    public void SendMessageToUser(UnoUser unoUser,String topic, Object object){
        SendMessage("user/" + unoUser.getUuid() + "/" + topic, object);
    }
    public void SendMessageToAllUsers(String topic, Object object){
        SendMessage("user/" + topic, object);
    }
    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("[MQTT] connection lost! " + cause.getMessage());
        cause.printStackTrace();
        System.out.println("[MQTT] reconnecting..");
        webSocketService.connect();

    }

    @Override
    public void messageArrived(String topic, MqttMessage message)  {
            topicCallbacks.forEach(tc -> {

                if (tc.getTopic().equals(topic)) tc.getTypeListeners().forEach(tl -> {
                    ObjectMapper mapper = new ObjectMapper();
                    WebSocketBaseMessage wbm = null;
                    try {
                        wbm = mapper.readValue(message.toString(), WebSocketBaseMessage.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("INVALID OBJECT TYPE");
                        wbm = new WebSocketBaseMessage(null,message.toString());
                    }
                    Object o = null;
                    try {
                        if(tl.getC() == String.class)
                            o = wbm.getObject();
                        else {
                            o = mapper.readValue(wbm.getObject(), tl.getC());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    //GET USER
                    UnoUser unoUser = null;
                    if(wbm.getToken() != null) {
                        try {
                            long id = tokenProvider.getUserIdFromToken(wbm.getToken());
                            unoUser = userRepo.findById(id);


                        }
                        catch (Exception ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                    if(unoUser !=null) {
                        tl.getSocketListener().notify(new WebSocketMessage(topic, o, unoUser));
                    }
                    else{
                        System.out.println("NULL USER REQUEST");
                    }
                });
            });
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}
