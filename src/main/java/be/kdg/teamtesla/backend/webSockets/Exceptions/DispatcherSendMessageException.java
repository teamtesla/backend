package be.kdg.teamtesla.backend.webSockets.Exceptions;

public class DispatcherSendMessageException extends RuntimeException {
    public DispatcherSendMessageException(String message) {
        super(message);
    }

    public DispatcherSendMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}
