package be.kdg.teamtesla.backend.webSockets.Exceptions;

public class MqttSubscribeException extends RuntimeException {
    public MqttSubscribeException(String message) {
        super(message);
    }

    public MqttSubscribeException(String message, Throwable cause) {
        super(message, cause);
    }
}
