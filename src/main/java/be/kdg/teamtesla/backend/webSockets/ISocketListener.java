package be.kdg.teamtesla.backend.webSockets;

public interface ISocketListener {
     void notify(WebSocketMessage wsm);
}
