package be.kdg.teamtesla.backend.webSockets;

public class TypeListener {
    private Class c;
    private ISocketListener SocketListener;

    public TypeListener() {
    }

    public TypeListener(Class c, ISocketListener iSocketListener) {
        this.c = c;
        this.SocketListener = iSocketListener;
    }

    public Class getC() {
        return c;
    }

    public void setType(Class c) {
        this.c = c;
    }

    public ISocketListener getSocketListener() {
        return SocketListener;
    }

    public void setSocketListener(ISocketListener socketListener) {
        this.SocketListener = socketListener;
    }
}
