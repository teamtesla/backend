package be.kdg.teamtesla.backend.webSockets.Exceptions;

public class MqttConnectionException extends RuntimeException {
    public MqttConnectionException(String message) {
        super(message);
    }

    public MqttConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
