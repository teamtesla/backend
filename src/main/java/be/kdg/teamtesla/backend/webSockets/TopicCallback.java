package be.kdg.teamtesla.backend.webSockets;

import java.util.ArrayList;
import java.util.List;

public class TopicCallback {
    private String topic;
    private List<TypeListener> typeListeners;

    public TopicCallback() {
        typeListeners = new ArrayList<>();
    }

    public TopicCallback(String topic) {
        this();
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }
    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<TypeListener> getTypeListeners() {
        return typeListeners;
    }

    public void setTypeListeners(List<TypeListener> typeListeners) {
        this.typeListeners = typeListeners;
    }
}
