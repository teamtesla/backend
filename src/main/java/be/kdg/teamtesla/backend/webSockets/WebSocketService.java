package be.kdg.teamtesla.backend.webSockets;

import be.kdg.teamtesla.backend.webSockets.Exceptions.MqttConnectionException;
import be.kdg.teamtesla.backend.webSockets.Exceptions.MqttSendMessageException;
import be.kdg.teamtesla.backend.webSockets.Exceptions.MqttSubscribeException;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class WebSocketService {


    //private String broker = "ws://51.83.44.226";
   private String broker = "ws://localhost";
    //private String broker = "ws://broker.hivemq.com";
    private int brokerPort = 8000;

    MemoryPersistence persistence = new MemoryPersistence();
    MqttClient client;




    public WebSocketService(){
        connect();
    }

    public void connect(){
        try {
            client = new MqttClient(broker + ":" + brokerPort, UUID.randomUUID().toString(),persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setAutomaticReconnect(true);
            connOpts.setCleanSession(true);
            connOpts.setMaxInflight(1000);
            client.connect(connOpts);
            System.out.println("[MQTT] connected.");
        }catch (Exception ex) {
            throw new MqttConnectionException(ex.getMessage());
        }
    }
    public void setCallback(MqttCallback callback){
        this.client.setCallback(callback);
    }
    public void subscribe(String topic){
        try {
            client.subscribe(topic);
        } catch (MqttException e) {
            throw new MqttSubscribeException(e.getMessage());
        }
    }

    public void sendMessage(String topic, String content){
        try {
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(2);
            client.publish(topic, message);

        } catch (MqttException e) {
            throw new MqttSendMessageException(e.getMessage());
        }
    }

}
