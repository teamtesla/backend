package be.kdg.teamtesla.backend.game;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.Player;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GameService {

    private final GameRepo gameRepo;

    public GameService(GameRepo gameRepo) {
        this.gameRepo = gameRepo;
    }

    public Game createGame(Game game){return gameRepo.save(game);}

    public List<Game> getGames(){return gameRepo.findAll();}

    public void removeGames() {gameRepo.deleteAll();}

    public Game getGameByUuid(String uuid) {
        return gameRepo.findGameByUuid(uuid);
    }

    public Game getGameById(long id) {
        return gameRepo.findGameByGameId(id);
    }

    public List<Game> gamesByLobby(Lobby lobby) {
        return gameRepo.findByLobby(lobby);
    }
    public void addPlayers(Game game,List<Player> players){
        game.setPlayers(players);
    }
}
