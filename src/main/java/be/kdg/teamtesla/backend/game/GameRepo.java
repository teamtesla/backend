package be.kdg.teamtesla.backend.game;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepo extends JpaRepository<Game,Long> {
    Game findGameByGameId(long gameId);
    Game findGameByUuid(String uuid);
    List<Game> findByLobby(Lobby lobby);
}
