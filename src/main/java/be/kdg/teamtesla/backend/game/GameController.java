package be.kdg.teamtesla.backend.game;

import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.game.dto.GameDTO;
import be.kdg.teamtesla.backend.game.dto.GameListDTO;
import be.kdg.teamtesla.backend.game.dto.TurnDTO;
import be.kdg.teamtesla.backend.lobby.LobbyController;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.Player;
import be.kdg.teamtesla.backend.player.PlayerService;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
public class GameController {
    private UnoUserService unoUserService;
    private GameService gameService;
    private WebsocketDispatcher websocketDispatcher;
    private LobbyService lobbyService;
    private PlayerService playerService;
    private GameManager gameManager;
    private LobbyController lobbyController;

    public GameController(GameService gameService, WebsocketDispatcher websocketDispatcher, LobbyService lobbyService, PlayerService playerService, GameManager gameManager, LobbyController lobbyController, UnoUserService unoUserService) {
        this.gameService = gameService;
        this.websocketDispatcher = websocketDispatcher;
        this.lobbyService = lobbyService;
        this.playerService = playerService;
        this.gameManager = gameManager;
        this.lobbyController = lobbyController;
        this.unoUserService = unoUserService;
        createSubscriptions();
    }

    private void createSubscriptions(){
        //Nieuwe game aanmaken
        websocketDispatcher.SubscribeOnTopic("game/create", new ISocketListener() {
            @Override
            @Transactional
            public void notify(WebSocketMessage wsm) {
                if(wsm.getUser() != null){
                    Lobby lobby = lobbyService.getLobbyById(Long.parseLong((wsm.getObject().toString().substring(5).replace("}", ""))));
                    if(lobby.getHost().getId() == wsm.getUser().getId()) {
                        if (lobby.getCurrentGame() != null) {
                            websocketDispatcher.SendMessageToUser(wsm.getUser(),"game/start",new ModelMapper().map(lobby.getCurrentGame(),GameDTO.class));
                        }
                        else {
                            long gameId = createGame(lobby.getLobbyId());
                            Game game = gameService.getGameById(gameId);
                            lobby.setCurrentGame(game);
                            lobbyService.update(lobby);
                            UnoUser uu = wsm.getUser();
                            uu.setCurrentGame(game);
                            unoUserService.saveUser(uu);
                            for (UnoUser unoUser : lobby.getUnoUsers()) {
                                GameDTO gameDTO = new ModelMapper().map(game,GameDTO.class);
                                gameDTO.setLobby(lobby.getLobbyId());
                                unoUser.setCurrentGame(game);
                                unoUserService.saveUser(unoUser);
                                //notify users game started
                                websocketDispatcher.SendMessageToUser(unoUser,"game/start",gameDTO);
                            }
                        }
                    }
                }
            }
        }, String.class);

        //process game actie van speler
        websocketDispatcher.SubscribeOnTopic("game/turn", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if (wsm.getUser() != null) {
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();
                        System.out.println(wsm.getObject().toString());
                        TurnDTO turnDTO = objectMapper.readValue(wsm.getObject().toString(), TurnDTO.class);
                        Optional<Game> optionalGame;
                        if (turnDTO.getChoiceColor() != null) {
                            optionalGame = gameManager.playATurn(turnDTO.getGameId(), turnDTO.getUserId(), turnDTO.getCardId(), Color.valueOf(turnDTO.getChoiceColor().toUpperCase()), turnDTO.isUno());
                        }
                        else {
                            optionalGame = gameManager.playATurn(turnDTO.getGameId(), turnDTO.getUserId(), turnDTO.getCardId(), null, turnDTO.isUno());
                        }
                        while (optionalGame.isPresent()) {
                            Game game = optionalGame.get();
                            for (Player player : game.getPlayers()) {
                                if (!player.isComputer()) {
                                    websocketDispatcher.SendMessageToUser(player.getUnoUser(), "game/turn", new GameDTO(player,game));
                                }
                            }
                            optionalGame = gameManager.checkComputer(game);
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }, String.class);

        //get game list
        //TODO:dto's gebruiken
        websocketDispatcher.SubscribeOnTopic("game/list", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if (wsm.getUser() != null) {
                    Lobby lobby = wsm.getUser().getCurrentLobby();
                    if (lobby != null) {
                        List<Game> games = gameService.gamesByLobby(lobby);
                        List<GameDTO> gameDTOS = new ArrayList<>();
                        games.forEach(game -> game.getPlayers().stream().filter(player -> player.getUnoUser() != null).filter(player -> player.getUnoUser().getId() == wsm.getUser().getId()).findAny().ifPresent(player -> gameDTOS.add(new GameDTO(player,game))));
                        websocketDispatcher.SendMessageToUser(wsm.getUser(), "game/list", new GameListDTO(gameDTOS));
                    }
                }
            }
        }, String.class);

        //get single game
        websocketDispatcher.SubscribeOnTopic("game/get", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                if (wsm.getUser() != null) {
                    GameDTO gameDTO = new GameDTO(wsm.getUser());
                    websocketDispatcher.SendMessageToUser(wsm.getUser(), "game/current", gameDTO);
                }
            }
        }, String.class);
    }





    public long createGame(long lobbyId) {
        Lobby lobby = lobbyService.getLobbyById(lobbyId);
        Game game = gameManager.createGame(lobby);
        return game.getGameId();
    }
}
