package be.kdg.teamtesla.backend.game.dto;

import java.util.List;

public class GameListDTO {
    private List<GameDTO> games;

    public GameListDTO() {
    }

    public GameListDTO(List<GameDTO> games) {
        this.games = games;
    }

    public List<GameDTO> getGames() {
        return games;
    }

    public void setGames(List<GameDTO> games) {
        this.games = games;
    }
}
