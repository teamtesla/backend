package be.kdg.teamtesla.backend.game;


import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.history.History;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.Player;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long gameId;
    // CascadeType.MERGE,
    @ManyToMany()
    @JoinTable(
            name = "Gamedeck_Card",
            joinColumns = { @JoinColumn(name = "game_id") },
            inverseJoinColumns = { @JoinColumn(name = "card_id") }
    )
    private List<Card> deck;
    // ,CascadeType.PERSIST
    @ManyToMany()
    @JoinTable(
            name = "Gamediscard_Card",
            joinColumns = { @JoinColumn(name = "game_id") },
            inverseJoinColumns = { @JoinColumn(name = "card_id") }
    )
    private List<Card> discard;
    //, CascadeType.PERSIST, CascadeType.REMOVE
    @OneToMany(mappedBy = "game", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Player> players;
    //, CascadeType.PERSIST
    @ManyToOne()
    @JoinColumn(name = "lobby_id", nullable = false)
    private Lobby lobby;
    @OneToOne(mappedBy = "currentGame")
    private Lobby currentGameLobby;
    private UUID uuid;
    private int amount;
    //Keeps track of what player's turn it is
    private int turn;
    @Enumerated(EnumType.ORDINAL)
    private Color color;
    private boolean clockwise;
    private boolean finished;


    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    private History history;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Game(Lobby lobby, List<Card> allCards, List<Player> players) {
        this.deck = allCards;
        this.lobby = lobby;
        this.players=players;
        this.clockwise=true;
        this.turn=0;
        this.discard=new ArrayList<>();
        this.finished = false;
    }

    public Game() {
        uuid = UUID.randomUUID();
    }

    public Card getLastCard(){
        Card lastCard=null;
        for (Card card : discard) {
            lastCard = card;
        }
        return lastCard;
    }

    public void addPlayers(Player player){
        players.add(player);
    }

    @JsonIgnore
    public Card getTopDiscardCard(){
        return discard.get(discard.size()-1);
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public List<Card> getDiscard() {
        return discard;
    }

    public void setDiscard(List<Card> discard) {
        this.discard = discard;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public boolean isClockwise() {
        return clockwise;
    }

    public void setClockwise(boolean clockwise) {
        this.clockwise = clockwise;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }


}
