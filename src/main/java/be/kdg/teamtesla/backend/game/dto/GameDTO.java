package be.kdg.teamtesla.backend.game.dto;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.player.Player;
import be.kdg.teamtesla.backend.player.dto.PlayerDTO;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GameDTO {
    private long gameId;
    private List<PlayerDTO> players;
    private Card topCard;
    private long lobby;
    private Color color;

    public GameDTO() {
    }

    public GameDTO(long gameId, List<PlayerDTO> players, Card topCard) {
        this.gameId = gameId;
        this.players = players;
        this.topCard = topCard;
    }

    public GameDTO(UnoUser user) {
        setGameId(user.getCurrentGame().getGameId());
        this.players = new ArrayList<>();
        for (Player player : user.getCurrentGame().getPlayers()) {
            if (player.isComputer()) {
                players.add(new PlayerDTO(player.getHand().size(), 0, player.isTurn(), "Computer"));
            } else if (player.getUnoUser().getUuid().equals(user.getUuid())) {
                players.add(new PlayerDTO(player));
            } else
                players.add(new PlayerDTO(player.getHand().size(), player.getUnoUser().getId(), player.isTurn(), player.getUnoUser().getUsername()));
        }
        this.topCard = user.getCurrentGame().getTopDiscardCard();
        this.color = user.getCurrentGame().getColor();
    }

    public GameDTO(Player gamePlayer, Game game) {
        setGameId(game.getGameId());
        this.players = new ArrayList<>();
        for (Player player : game.getPlayers()) {
            if (player.equals(gamePlayer)) {
                players.add(new PlayerDTO(player));
            } else if (player.isComputer()) {
                players.add(new PlayerDTO(player.getHand().size(), 0, player.isTurn(), "Computer"));
            } else
                players.add(new PlayerDTO(player.getHand().size(), player.getUnoUser().getId(), player.isTurn(), player.getUnoUser().getUsername()));
        }
        this.topCard = game.getTopDiscardCard();
        this.color = game.getColor();
    }

    public List<PlayerDTO> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDTO> players) {
        this.players = players;
    }

    public Card getTopCardId() {
        return topCard;
    }

    public void setTopCardId(Card topCard) {
        this.topCard = topCard;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public Card getTopCard() {
        return topCard;
    }

    public void setTopCard(Card topCard) {
        this.topCard = topCard;
    }

    public long getLobby() {
        return lobby;
    }

    public void setLobby(long lobby) {
        this.lobby = lobby;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
