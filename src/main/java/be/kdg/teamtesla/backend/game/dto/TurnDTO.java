package be.kdg.teamtesla.backend.game.dto;

public class TurnDTO {
    private long gameId;
    private long cardId;
    private long userId;
    private boolean uno;
    private String choiceColor;
    private boolean takeCard;

    public boolean isTakeCard() {
        return takeCard;
    }

    public void setTakeCard(boolean takeCard) {
        this.takeCard = takeCard;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isUno() {
        return uno;
    }

    public void setUno(boolean uno) {
        this.uno = uno;
    }

    public String getChoiceColor() {
        return choiceColor;
    }

    public void setChoiceColor(String choiceColor) {
        this.choiceColor = choiceColor;
    }
}
