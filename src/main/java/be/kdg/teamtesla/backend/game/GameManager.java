package be.kdg.teamtesla.backend.game;

import be.kdg.teamtesla.backend.ai.Turn;
import be.kdg.teamtesla.backend.ai.UnoAI;
import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.CardService;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.card.Value;
import be.kdg.teamtesla.backend.game.exception.CardNotInHandException;
import be.kdg.teamtesla.backend.game.exception.NoColorException;
import be.kdg.teamtesla.backend.game.exception.UnplayableCardException;
import be.kdg.teamtesla.backend.game.exception.WrongUserException;
import be.kdg.teamtesla.backend.history.HistoryManager;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.Player;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
public class GameManager {

    private GameService gameService;
    private CardService cardService;
    private UnoUserService userService;
    private UnoAI unoAI;
    private HistoryManager historyManager;

    public GameManager(GameService gameService, CardService cardService, UnoAI unoAI, HistoryManager historyManager, UnoUserService userService) {
        this.gameService = gameService;
        this.cardService = cardService;
        this.unoAI = unoAI;
        this.userService = userService;
        this.historyManager = historyManager;
    }


    @Transactional
    public Game createGame(Lobby lobby) {
        List<Card> cardSet = cardService.getAllCards();
        List<Player> players = new ArrayList<>();
        for (UnoUser unoUser : lobby.getUnoUsers()) {
            players.add(new Player(unoUser));
        }
        if (lobby.getPlayerCount() > lobby.getUnoUsers().size()) {
            for (int i = 0; i < lobby.getPlayerCount() - lobby.getUnoUsers().size(); i++) {
                players.add(new Player());
            }
        }
        Game game = new Game(lobby, cardSet, players);

        game.getDiscard().add(drawDiscardCard(game.getDeck(), game));

        for (Player player : game.getPlayers()) {
            game.setAmount(game.getAmount() + 1);
            player.setGame(game);
            for (int i = 0; i < 7; i++) {
                player.getHand().add(drawACard(game.getDeck(), game));
            }
        }
        initialCard(game);
        game.getPlayers().get(game.getTurn()).setTurn(true);

        game.setHistory(historyManager.createHistory(game));

        Game returnGame = gameService.createGame(game);
//        Optional<Game> optionalGame = checkComputer(returnGame);
//        while (optionalGame.isPresent()) {
//            returnGame = optionalGame.get();
//            optionalGame = checkComputer(game);
//        }
        return returnGame;
    }

    public Card drawACard(List<Card> deck, Game game) {
        int deckSize = deck.size();
        Random rng = new Random();
        int i = 0;
        int randomNumber = rng.nextInt(deckSize);
        Card drawnCard;
        drawnCard = deck.get(randomNumber);
        deck.remove(drawnCard);
        if (deck.size() == 0) {
            refillDeck(game);
        }
        return drawnCard;
    }

    private void refillDeck(Game game) {
        List<Card> newDeck = game.getDiscard();
        Card topCard = newDeck.get(newDeck.size() - 1);
        newDeck.remove(topCard);
        Collections.shuffle(newDeck);
        game.setDeck(newDeck);
        List<Card> newDiscard = new ArrayList<Card>();
        newDiscard.add(topCard);
        game.setDiscard(newDiscard);
    }

    public Card drawDiscardCard(List<Card> deck, Game game) {
        Card drawnCard = null;
        while (drawnCard == null || drawnCard.getColor().equals(Color.BLACK)) {
            drawnCard = drawACard(deck, game);
        }
        return drawnCard;
    }

    @Transactional
    public Optional<Game> playATurn(long gameId, long userId, long cardId, Color choiceColor, boolean calledUno) {
        Game game = gameService.getGameById(gameId);
        if (game.isFinished()) {
            return Optional.empty();
        }
        Card card;
        if (cardId != 999) {
            card = cardService.getCardById(cardId);
        } else card = null;
        Player player;
        if (game.getPlayers().get(game.getTurn()).getUnoUser().getId() != userId) {
            throw new WrongUserException();
        } else {
            player = game.getPlayers().get(game.getTurn());
        }
        if (!isLegalCard(game, player, card) && card != null) {
            return Optional.empty();
        }
        int turn = game.getTurn();
        if (card == null) {
            playACard(game, player, card, choiceColor, false);
        } else if (game.getTopDiscardCard().getValue().equals(card.getValue()) || game.getTopDiscardCard().getColor().equals(card.getColor()) || card.getColor().equals(Color.BLACK)) {
            playACard(game, player, card, choiceColor, calledUno);
        } else if (game.getTopDiscardCard().getColor().equals(Color.BLACK)) {
            if (game.getColor() == null) {
                playACard(game, player, card, choiceColor, calledUno);
            } else {
                if (game.getColor().equals(card.getColor())) {
                    playACard(game, player, card, choiceColor, calledUno);
                } else throw new UnplayableCardException();
            }
        }
        gameFinished(game, turn);
        setTurn(game);
        return Optional.of(gameService.createGame(game));
    }

    /* checks if the game is clockwise
     * Makes sure turncounter doesnt underflow or overflow
     * Turnskips by doing this function twice
     * */
    private int endTurn(boolean clockwise, int currentTurn, int amount) {
        amount--;
        if (clockwise) {
            if (currentTurn == amount) {
                currentTurn = 0;
            } else currentTurn++;
        } else if (currentTurn == 0) {
            currentTurn = amount;
        } else currentTurn--;
        return currentTurn;
    }

    private Player getNextPlayer(Game game) {
        return game.getPlayers().get(endTurn(game.isClockwise(), game.getTurn(), game.getAmount()));
    }

    //Sets starting card for game
    public void initialCard(Game game) {
        if (game.getTopDiscardCard().getValue().equals(Value.SKIP)) {
            game.setTurn(endTurn(game.isClockwise(), game.getTurn(), game.getAmount()));
        } else if (game.getTopDiscardCard().getValue().equals(Value.REVERSE)) {
            game.setClockwise(!game.isClockwise());
            game.setTurn(endTurn(game.isClockwise(), game.getTurn(), game.getAmount()));
        } else if (game.getTopDiscardCard().getValue().equals(Value.TAKETWO)) {
            game.getPlayers().get(game.getTurn()).getHand().add(drawACard(game.getDeck(), game));
            game.getPlayers().get(game.getTurn()).getHand().add(drawACard(game.getDeck(), game));
            game.setTurn(endTurn(game.isClockwise(), game.getTurn(), game.getAmount()));
        }
    }

    private void playACard(Game game, Player player, Card card, Color choiceColor, boolean calledUno) {
        if (card == null) {
            drawCardDraw(1,game,player,game.getTurn());
            game.setTurn(endTurn(game.isClockwise(),game.getTurn(),game.getAmount()));
            return;
        }
        if (card.getColor() == Color.BLACK && choiceColor == null) {
            throw new NoColorException();
        }

        game.getHistory().addTurn(historyManager.createTurn(card,game.getTurn(),true, game.getHistory()));
        int turn = game.getTurn();

        if (choiceColor != null && (card.getValue().equals(Value.CHANGE) || card.getValue().equals(Value.TAKEFOUR))) {
            game.setColor(choiceColor);
            if(card.getValue().equals(Value.TAKEFOUR)){
                Player add4=getNextPlayer(game);
                game.setTurn(endTurn(game.isClockwise(),game.getTurn(),game.getAmount()));
                drawCardDraw(4,game,add4,game.getTurn());
            }
        } else if (card.getValue().equals(Value.REVERSE)) {
            game.setClockwise(!game.isClockwise());
        }else if (card.getValue().equals(Value.SKIP)){
            game.setTurn(endTurn(game.isClockwise(),game.getTurn(),game.getAmount()));
        }else if (card.getValue().equals(Value.TAKETWO)){
            Player add2=getNextPlayer(game);
            game.setTurn(endTurn(game.isClockwise(),game.getTurn(),game.getAmount()));
            drawCardDraw(2,game,add2,game.getTurn());
        }
        game.getDiscard().add(card);
        if (player.getHand().size()==2){
            checkUno(game,player,calledUno,turn);
        }
        player.getHand().remove(card);
        game.setTurn(endTurn(game.isClockwise(), game.getTurn(), game.getAmount()));
    }

    private void gameFinished(Game game, int turn) {
        if (game.getPlayers().get(turn).getHand().size() == 0) {
            game.setFinished(true);
            List<Player> players = game.getPlayers();
            int totalScore = 0;
            for (Player player : players) {
                Set<Card> playerHand = player.getHand();
                for (Card card : playerHand) {
                    totalScore = totalScore + card.getValue().getScore();
                }
                if (player.getUnoUser() != null) {
                    player.getUnoUser().getUserStats().playedGame();
                    userService.saveUser(player.getUnoUser());
                }
            }
            if (game.getPlayers().get(turn).getUnoUser() != null) {
                game.getPlayers().get(turn).getUnoUser().getUserStats().addPoints(totalScore);
                game.getPlayers().get(turn).getUnoUser().getUserStats().wonGame();
                userService.saveUser(game.getPlayers().get(turn).getUnoUser());
            }
        }
    }

    private void checkUno(Game game,Player player,boolean calledUno, int turn) {
        if(!calledUno){
            drawCardDraw(2,game,player,turn);
        }
    }

    private boolean isLegalCard(Game game, Player player, Card card) {
        if (!game.getPlayers().contains(player)) {
            throw new CardNotInHandException();
        }
        if (!player.getHand().contains(card) && card != null) {
            throw new CardNotInHandException();
        }
        return true;
    }

    Optional<Game> checkComputer(Game game) {
        if (!game.isFinished() && game.getPlayers().stream().anyMatch(player -> player.isComputer() && player.isTurn())) {
            int turnCount = game.getTurn();
            Turn turn = unoAI.playTurn(game.getTopDiscardCard(), new ArrayList<>(game.getPlayers().stream().filter(Player::isTurn).findAny().get().getHand()), game.getColor());

            if (turn.getCard() == null) System.out.println("[AI] " + turnCount + "took some cards");
            else
                System.out.println("[AI] " + turnCount + ": " + turn.getCard().getValue() + " " + turn.getCard().getColor());

            playACard(game, game.getPlayers().get(game.getTurn()), turn.getCard(), turn.getChoice(), true);
            gameFinished(game, turnCount);
            setTurn(game);
            int x = 0;
                while (x<10) {
                    try {
                        return Optional.of(gameService.createGame(game));
                    } catch (Exception ex) {
                        x++;
                    }
                }
                throw new RuntimeException("Persistence error.");
        } else return Optional.empty();
    }

    private void setTurn(Game game) {
        for (int i = 0; i < game.getPlayers().size(); i++) {
            if (i == game.getTurn()) {
                game.getPlayers().get(i).setTurn(true);
            } else game.getPlayers().get(i).setTurn(false);
        }
    }

    private void drawCardDraw(int amount, Game game, Player player, int turn) {
        for (int i = 0; i < amount; i++) {
            Card drawCard = drawACard(game.getDeck(), game);
            player.getHand().add(drawCard);
            game.getHistory().addTurn(historyManager.createTurn(drawCard, turn, false, game.getHistory()));
        }
    }
}
