package be.kdg.teamtesla.backend.friend.dto;


public class FriendRequestDTO {

    private String sender;
    private String receiver;
    private long friendRequestId;

    public FriendRequestDTO() {
    }

    public FriendRequestDTO(String sender, String receiver, long friendRequestId) {
        this.sender = sender;
        this.receiver = receiver;
        this.friendRequestId = friendRequestId;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public long getFriendRequestId() {
        return friendRequestId;
    }

    public void setFriendRequestId(long friendRequestId) {
        this.friendRequestId = friendRequestId;
    }
}
