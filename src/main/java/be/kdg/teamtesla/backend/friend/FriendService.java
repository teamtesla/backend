package be.kdg.teamtesla.backend.friend;

import be.kdg.teamtesla.backend.friend.model.FriendRequest;
import be.kdg.teamtesla.backend.friend.repo.FriendRequestRepo;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FriendService {
    private FriendRequestRepo friendRequestRepo;
    private UserRepo userRepo;

    public FriendService(FriendRequestRepo friendRequestRepo, UserRepo userRepo) {
        this.friendRequestRepo = friendRequestRepo;
        this.userRepo = userRepo;
    }
    public List<FriendRequest> findFriendRequestsByUser(UnoUser user){
        return friendRequestRepo.findByReceiver(user);
    }

    public Optional<FriendRequest> newFriendRequest(FriendRequest friendRequest){
        return Optional.of(friendRequestRepo.save(friendRequest));
    }
    public void deleteAllFriendRequests(){
        friendRequestRepo.deleteAll();
    }
    public void removeFriendRequest(FriendRequest friendRequest){
        friendRequestRepo.deleteByFriendRequestId(friendRequest.getFriendRequestId());
    }
    public Optional<FriendRequest> findFriendRequestsByReceiverAndSender(UnoUser receiver, UnoUser sender){
        return friendRequestRepo.findByReceiverAndSender(receiver,sender);
    }

    public Optional<FriendRequest> findByFriendRequestId(long friendRequestId) {
        return friendRequestRepo.findByFriendRequestId(friendRequestId);
    }
}
