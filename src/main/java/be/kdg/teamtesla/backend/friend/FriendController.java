package be.kdg.teamtesla.backend.friend;

import be.kdg.teamtesla.backend.friend.dto.FriendListDTO;
import be.kdg.teamtesla.backend.friend.dto.FriendRequestDTO;
import be.kdg.teamtesla.backend.friend.dto.FriendRequestListDTO;
import be.kdg.teamtesla.backend.friend.model.FriendRequest;
import be.kdg.teamtesla.backend.game.GameManager;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@Transactional
public class FriendController {
    private UnoUserService userService;
    private LobbyService lobbyService;
    private GameManager gameManager;
    private WebsocketDispatcher websocketDispatcher;
    private FriendManager friendManager;
    private FriendService friendService;

    public FriendController(LobbyService lobbyService, GameManager gameController, WebsocketDispatcher websocketDispatcher, UnoUserService unoUserService, FriendManager friendManager, FriendService friendService) {
        this.lobbyService = lobbyService;
        this.gameManager=gameController;
        this.websocketDispatcher = websocketDispatcher;
        this.userService = unoUserService;
        this.friendManager = friendManager;
        this.friendService = friendService;
        createSubscriptions();
    }
    private void createSubscriptions(){
        websocketDispatcher.SubscribeOnTopic("friend/request/make", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                FriendRequestDTO friendRequestDTO = (FriendRequestDTO) wsm.getObject();
                Optional<UnoUser> u = userService.findByUserEmail(friendRequestDTO.getReceiver());
                if(u.isPresent()){
                    UnoUser sender = wsm.getUser();
                    UnoUser receiver = u.get();
                    friendManager.makeRequest(sender,receiver).ifPresent(friendRequest -> {
                        friendRequestDTO.setSender(sender.getUsername());
                        friendRequestDTO.setFriendRequestId(friendRequest.getFriendRequestId());
                        websocketDispatcher.SendMessageToUser(receiver,"friend/request/new", friendRequestDTO);
                    });
                }
            }
        }, FriendRequestDTO.class);
        websocketDispatcher.SubscribeOnTopic("friend/request/list", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                List<FriendRequest> friendRequests = friendManager.getAllFriendRequests(wsm.getUser());
                FriendRequestListDTO friendRequestListDTO = new FriendRequestListDTO();
                friendRequestListDTO.setFriendRequests(new ArrayList<>());
                friendRequests.forEach(friendRequest -> {
                    friendRequestListDTO.getFriendRequests().add(new FriendRequestDTO(friendRequest.getSender().getUsername(),friendRequest.getReceiver().getEmail(),friendRequest.getFriendRequestId()));
                });
                websocketDispatcher.SendMessageToUser(wsm.getUser(),"friend/request/list", friendRequestListDTO);
            }
        }, String.class);
        //sender now becomes receiver
        //TODO:check this code I wrote when very sleepy
        websocketDispatcher.SubscribeOnTopic("friend/request/accept", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                FriendRequestDTO friendRequestDTO = (FriendRequestDTO)wsm.getObject();
                friendManager.acceptFriendRequest(friendRequestDTO.getFriendRequestId(),friendRequestDTO.getReceiver());
            }
        }, FriendRequestDTO.class);
        websocketDispatcher.SubscribeOnTopic("friend/request/decline", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                FriendRequestDTO friendRequestDTO = (FriendRequestDTO)wsm.getObject();
                friendManager.declineFriendRequest(friendRequestDTO.getFriendRequestId(),friendRequestDTO.getReceiver());
            }
        }, FriendRequestDTO.class);
        websocketDispatcher.SubscribeOnTopic("friend/remove", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                Optional<UnoUser> u = userService.findByUserEmail((String)wsm.getObject());
                u.ifPresent(unoUser -> friendManager.undoFriends(wsm.getUser(), unoUser));
            }
        },String.class);
        websocketDispatcher.SubscribeOnTopic("friend/list", new ISocketListener() {
            @Override
            public void notify(WebSocketMessage wsm) {
                FriendListDTO friendListDTO = new FriendListDTO();
                Type listType = new TypeToken<List<UnoUserDTO>>() {}.getType();
                List<UnoUserDTO> friends = new ModelMapper().map(wsm.getUser().getFriends(),listType);
                friendListDTO.setFriendlist(friends);
                websocketDispatcher.SendMessageToUser(wsm.getUser(),"friend/list",friendListDTO);
            }
        }, String.class);
    }

}
