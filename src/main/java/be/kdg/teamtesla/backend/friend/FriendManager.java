package be.kdg.teamtesla.backend.friend;

import be.kdg.teamtesla.backend.friend.model.FriendRequest;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class FriendManager {
    private FriendService friendService;
    private UnoUserService unoUserService;

    public FriendManager(FriendService friendService, UnoUserService unoUserService) {
        this.friendService = friendService;
        this.unoUserService = unoUserService;
    }

    public void makeFriends(UnoUser A, UnoUser B){
        A.getFriends().add(B);
        B.getFriends().add(A);

        unoUserService.saveUser(A);
        unoUserService.saveUser(B);
    }
    public void undoFriends(UnoUser A, UnoUser B){
        A.getFriends().remove(B);
        B.getFriends().remove(A);
    }
    public Optional<FriendRequest> makeRequest(UnoUser sender, UnoUser receiver){
        if (sender.getEmail().equals(receiver.getEmail())) return Optional.empty();
        Optional<FriendRequest> optionalFriendRequest = friendService.findFriendRequestsByReceiverAndSender(receiver,sender);
        if (!optionalFriendRequest.isPresent()) {
            Optional<FriendRequest> optionalReverse = friendService.findFriendRequestsByReceiverAndSender(sender,receiver);
            if (!optionalReverse.isPresent()) {
                FriendRequest friendRequest = new FriendRequest();
                friendRequest.setSender(sender);
                friendRequest.setReceiver(receiver);
                return friendService.newFriendRequest(friendRequest);
            }
        }
        return Optional.empty();
    }
    public void removeRequest(FriendRequest friendRequest){

    }
    public List<FriendRequest> getAllFriendRequests(UnoUser user){
        return friendService.findFriendRequestsByUser(user);
    }
    public void deleteAllFriendRequests(){
        friendService.deleteAllFriendRequests();
    }
    public void acceptFriendRequest(long friendRequestId, String receiver){
        Optional<FriendRequest> optionalFriendRequest = friendService.findByFriendRequestId(friendRequestId);
        if (optionalFriendRequest.isPresent() && optionalFriendRequest.get().getReceiver().getEmail().equals(receiver)) {
            makeFriends(optionalFriendRequest.get().getReceiver(),optionalFriendRequest.get().getSender());
            friendService.removeFriendRequest(optionalFriendRequest.get());
        }
    }
    public void declineFriendRequest(long friendRequestId, String receiver) {
        Optional<FriendRequest> optionalFriendRequest = friendService.findByFriendRequestId(friendRequestId);
        if (optionalFriendRequest.isPresent() && optionalFriendRequest.get().getReceiver().getEmail().equals(receiver)) {
            friendService.removeFriendRequest(optionalFriendRequest.get());
        }
    }
}
