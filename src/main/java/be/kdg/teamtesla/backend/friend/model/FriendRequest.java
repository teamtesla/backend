package be.kdg.teamtesla.backend.friend.model;

import be.kdg.teamtesla.backend.security.model.UnoUser;

import javax.persistence.*;

@Entity
public class FriendRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long friendRequestId;

    @ManyToOne
    private UnoUser sender;
    @ManyToOne
    private UnoUser receiver;

    public FriendRequest() {
    }

    public UnoUser getSender() {
        return sender;
    }

    public void setSender(UnoUser sender) {
        this.sender = sender;
    }

    public UnoUser getReceiver() {
        return receiver;
    }

    public void setReceiver(UnoUser receiver) {
        this.receiver = receiver;
    }

    public long getFriendRequestId() {
        return friendRequestId;
    }

    public void setFriendRequestId(long friendRequestId) {
        this.friendRequestId = friendRequestId;
    }
}
