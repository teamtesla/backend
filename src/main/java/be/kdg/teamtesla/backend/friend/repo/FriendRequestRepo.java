package be.kdg.teamtesla.backend.friend.repo;

import be.kdg.teamtesla.backend.friend.model.FriendRequest;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FriendRequestRepo extends JpaRepository<FriendRequest,Long> {
    Optional<FriendRequest> findByFriendRequestId(long friendRequestId);
    List<FriendRequest> findByReceiver(UnoUser user);
    void deleteAll();
    void deleteByFriendRequestId(long id);
    Optional<FriendRequest>findByReceiverAndSender(UnoUser receiver, UnoUser sender);
}
