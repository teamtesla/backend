package be.kdg.teamtesla.backend.friend.dto;

import be.kdg.teamtesla.backend.security.dto.UnoUserDTO;

import java.util.List;

public class FriendListDTO {
    private List<UnoUserDTO> friendlist;

    public List<UnoUserDTO> getFriendlist() {
        return friendlist;
    }

    public void setFriendlist(List<UnoUserDTO> friendlist) {
        this.friendlist = friendlist;
    }
}
