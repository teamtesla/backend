package be.kdg.teamtesla.backend.friend.dto;

import java.util.List;

public class FriendRequestListDTO {
    private List<FriendRequestDTO> friendRequests;

    public FriendRequestListDTO() {
    }

    public List<FriendRequestDTO> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequestDTO> friendRequests) {
        this.friendRequests = friendRequests;
    }
}
