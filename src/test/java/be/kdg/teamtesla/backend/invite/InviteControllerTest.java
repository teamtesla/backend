package be.kdg.teamtesla.backend.invite;

import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.security.TokenProvider;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.payload.AuthResponse;
import be.kdg.teamtesla.backend.security.payload.LoginRequest;
import be.kdg.teamtesla.backend.webSockets.WebSocketBaseMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class InviteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InvitationService invitationService;
    @Autowired
    private WebsocketDispatcher dispatcher;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UnoUserService unoUserService;

    private static String token = null;


    @Test
    public void A_getToken() throws Exception {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("jvanhoye@hotmail.com");
        loginRequest.setPassword("Abc123456");
        MvcResult result = mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(loginRequest.toString()))
                .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(result.getResponse().getContentAsString(),AuthResponse.class);
        token = authResponse.getAccessToken();
    }
    @Test
    public void B_clearInvites(){
        invitationService.removeAllInvites();
        assert invitationService.getInviteCount() == 0;
    }

    @Test
    public void D_doInvitations(){
        Lobby lobby = new Lobby();
        lobby.setPlayerCount(5);
        lobby.setName("[TEST]");
        lobby.setHost(unoUserService.findById(tokenProvider.getUserIdFromToken(token)));
        lobby.setIsPrivate(false);
        lobby.setUuid(UUID.randomUUID().toString());

        lobby = lobbyService.createLobby(lobby);

        WebSocketBaseMessage wsb = new WebSocketBaseMessage();
        wsb.setToken(token);
        InvitationDTO idto = new InvitationDTO();


        idto.setLobby(lobby.getLobbyId());
        List<String> receivers = new ArrayList<>();
        receivers.add("jvanhoye@hotmail.com");
        receivers.add("mathijs.hallo@gmail.com");
        idto.setReceivernames(receivers);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            wsb.setObject(objectMapper.writeValueAsString(idto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            assert false;
        }
        dispatcher.SendMessage("invitation/send",wsb);
        await().atMost(10, SECONDS).until(() -> invitationService.getInviteCount() > 0);
    }
}
