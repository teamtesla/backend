package be.kdg.teamtesla.backend.Websockets;

import be.kdg.teamtesla.backend.security.payload.AuthResponse;
import be.kdg.teamtesla.backend.security.payload.LoginRequest;
import be.kdg.teamtesla.backend.webSockets.ISocketListener;
import be.kdg.teamtesla.backend.webSockets.WebSocketBaseMessage;
import be.kdg.teamtesla.backend.webSockets.WebSocketMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class WebsocketDispatcherTest{

    @Autowired
     private MockMvc mockMvc;

    @Autowired
    private WebsocketDispatcher websocketDispatcher;
    private static String testString = "empty";
    private static String token = null;

//    @Test
//    public void C_ReceiveWebsocket(){
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        await().atMost(10, SECONDS).until(() -> testString.equals("message send from test"));
//    }

    @Test
    public void A_subscribeOnTopic() {
    websocketDispatcher.SubscribeOnTopic("World", new ISocketListener() {
        @Override
        public void notify(WebSocketMessage wsm) {
            testString = (String)wsm.getObject();
        }
    }, String.class);
    assert true;
    }
    @Test
    public void AA_getToken() throws Exception {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("jvanhoye@hotmail.com");
        loginRequest.setPassword("Abc123456");
        MvcResult result = mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(loginRequest.toString()))
                 .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(result.getResponse().getContentAsString(),AuthResponse.class);
        token = authResponse.getAccessToken();
    }

    @Test
    public void B_sendMessage() {
        WebSocketBaseMessage webSocketBaseMessage = new WebSocketBaseMessage("testest", "message send from test");

        websocketDispatcher.SendMessage("World", webSocketBaseMessage);
        assert true;
    }




}
