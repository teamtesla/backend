package be.kdg.teamtesla.backend.statistics;

import be.kdg.teamtesla.backend.card.CardService;
import be.kdg.teamtesla.backend.game.GameManager;
import be.kdg.teamtesla.backend.game.GameService;
import be.kdg.teamtesla.backend.lobby.LobbyController;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.security.UnoUserService;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class StatisticsTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GameService gameService;
    @Autowired
    private GameManager gameManager;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private LobbyController lobbyManager;
    @Autowired
    private CardService cardService;
    @Autowired
    private UnoUserService unoUserService;


    private static long gameId;

    private final long userId1=2L;
    private final long userId2=4L;

    @Transactional
    @Test
    public void test() {
        Assert.assertTrue(true);
    }

    //todo fix test
//    @Transactional
//    @Test
//    public void statisticsUpdateOnGamePlayed(){
//        Lobby lobby = lobbyManager.createLobby("finishAGame",userId1);
//        long lobbyId = lobby.getLobbyId();
//        lobbyManager.addUser(lobbyId,userId2);
//        gameId = gameManager.createGame(lobby).getGameId();
//        Game game = gameService.getGameById(gameId);
//        game.setTurn(0);
//        //Blue 9 on discard top
//        game.getDiscard().add(cardService.getCardById(44L));
//        //Blue 8 as last card in player 1 hand
//        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
//        game.getPlayers().get(0).getHand().add(cardService.getCardById(43L));
//        //Give player 2-4 70 points worth in their hands
//        game.getPlayers().get(1).getHand().removeAll(game.getPlayers().get(1).getHand());
//        game.getPlayers().get(1).getHand().add(cardService.getCardById(50L));
//        game.getPlayers().get(1).getHand().add(cardService.getCardById(107L));
//        game.getPlayers().get(2).getHand().removeAll(game.getPlayers().get(2).getHand());
//        game.getPlayers().get(2).getHand().add(cardService.getCardById(50L));
//        game.getPlayers().get(2).getHand().add(cardService.getCardById(107L));
//        game.getPlayers().get(3).getHand().removeAll(game.getPlayers().get(3).getHand());
//        game.getPlayers().get(3).getHand().add(cardService.getCardById(50L));
//        game.getPlayers().get(3).getHand().add(cardService.getCardById(107L));
//        gameManager.playATurn(gameId,game.getPlayers().get(0).getUnoUser().getId(),43L,null,false);
//        Assert.assertEquals(true,game.isFinished());
//        Assert.assertEquals(1,unoUserService.findById(userId1).getUserStats().getGamesPlayed());
//        Assert.assertEquals(1,unoUserService.findById(userId2).getUserStats().getGamesPlayed());
//        Assert.assertEquals(1,unoUserService.findById(userId1).getUserStats().getGamesWon());
//        Assert.assertEquals(210,unoUserService.findById(userId1).getUserStats().getPointsEarned());
//    }
}
