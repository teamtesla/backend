package be.kdg.teamtesla.backend.ai;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.card.Value;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UnoAITest {

    @Autowired
    private UnoAI unoAI;

    @Test
    public void playTurnWithColor() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLUE, Value.SKIP));
        hand.add(new Card(Color.RED, Value.REVERSE));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.ZERO));
        Turn turn = unoAI.playTurn(topCard,hand,null);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.SKIP);
    }

    @Test
    public void playTurnWithValue() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,null);
        Assert.assertEquals(turn.getCard().getValue(),Value.EIGHT);
    }

    @Test
    public void playTurnWithBlack() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,null);
        Assert.assertEquals(turn.getCard().getValue(),Value.CHANGE);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLACK);
        Assert.assertTrue(hand.stream().filter(card -> !card.getColor().equals(Color.BLACK)).anyMatch(card -> card.getColor().equals(turn.getChoice())));
    }

    @Test
    public void playTurnWithoutPlayableCard() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,null);
        Assert.assertNull(turn.getCard());
    }

    @Test
    public void playTurnAfterBlack() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLUE, Value.EIGHT));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.EIGHT);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLUE);
    }

    @Test
    public void playTurnAfterBlackWithoutPlayableCard() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertNull(turn.getCard());
    }

    @Test
    public void playTurnAfterBlackWithBlack() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.CHANGE);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLACK);
        Assert.assertTrue(hand.stream().filter(card -> !card.getColor().equals(Color.BLACK)).anyMatch(card -> card.getColor().equals(turn.getChoice())));
    }

    @Test
    public void playTurnWithBlackAndWrongColor() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.RED, Value.TAKETWO));
        hand.add(new Card(Color.RED, Value.FOUR));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.CHANGE);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLACK);
        Assert.assertEquals(turn.getChoice(),Color.RED);
    }

    @Test
    public void playTurnWithBlackOnly() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.CHANGE);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLACK);
        Assert.assertNotEquals(Color.BLUE,turn.getChoice());
    }

    @Test
    public void playTurnWithOnePlayableCardAndBlack() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        hand.add(new Card(Color.BLUE, Value.SKIP));
        Turn turn = unoAI.playTurn(topCard,hand,Color.BLUE);
        Assert.assertEquals(turn.getCard().getValue(),Value.SKIP);
        Assert.assertEquals(turn.getCard().getColor(),Color.BLUE);
    }

    @Test
    public void playableCardsWithColor() {
        Card topCard = new Card(Color.BLACK, Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.BLUE, Value.SKIP));
        hand.add(new Card(Color.RED, Value.REVERSE));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.ZERO));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,Color.GREEN);
        Assert.assertEquals(1,playableCards.size());
        Assert.assertEquals(playableCards.get(0).getColor(),Color.GREEN);
        Assert.assertEquals(playableCards.get(0).getValue(),Value.TAKETWO);
    }

    @Test
    public void playableCardsWithValue() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,null);
        Assert.assertEquals(3,playableCards.size());
        Assert.assertTrue(playableCards.stream().allMatch(card -> card.getValue().equals(Value.EIGHT)));
    }

    @Test
    public void playableCardsWithValueAndColor() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLUE, Value.SKIP));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,null);
        Assert.assertEquals(4,playableCards.size());
        Assert.assertTrue(playableCards.stream().allMatch(card -> card.getValue().equals(Value.EIGHT) || card.getColor().equals(Color.BLUE)));
    }

    @Test
    public void playableCardsWithBlack() {
        Card topCard = new Card(Color.BLUE, Value.SEVEN);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,null);
        Assert.assertEquals(1,playableCards.size());
        Assert.assertEquals(playableCards.get(0).getColor(),Color.BLACK);
        Assert.assertEquals(playableCards.get(0).getValue(),Value.CHANGE);
    }

    @Test
    public void playableCardsWithTakeFourAndPlayableCards() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,null);
        Assert.assertEquals(3,playableCards.size());
        Assert.assertTrue(playableCards.stream().allMatch(card -> card.getValue().equals(Value.EIGHT)));
    }

    @Test
    public void playableCardsMix() {
        Card topCard = new Card(Color.BLUE, Value.EIGHT);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.EIGHT));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLUE, Value.SKIP));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        List<Card> playableCards = unoAI.playableCards(hand,topCard,null);
        Assert.assertEquals(5,playableCards.size());
        Assert.assertTrue(playableCards.stream().allMatch(card -> card.getValue().equals(Value.EIGHT) || card.getColor().equals(Color.BLUE) || card.getValue().equals(Value.CHANGE)));
    }

    @Test
    public void chooseCardBlack() {
        List<Card> playableCards = new ArrayList<>();
        playableCards.add(new Card(Color.BLACK, Value.CHANGE));
        playableCards.add(new Card(Color.BLACK, Value.CHANGE));
        Card card = unoAI.chooseCard(playableCards);
        Assert.assertEquals(card.getValue(),Value.CHANGE);
        Assert.assertEquals(card.getColor(),Color.BLACK);
    }

    @Test
    public void chooseCardMix() {
        List<Card> playableCards = new ArrayList<>();
        playableCards.add(new Card(Color.RED, Value.EIGHT));
        playableCards.add(new Card(Color.GREEN, Value.EIGHT));
        playableCards.add(new Card(Color.YELLOW, Value.EIGHT));
        playableCards.add(new Card(Color.RED, Value.SKIP));
        playableCards.add(new Card(Color.GREEN, Value.TAKETWO));
        playableCards.add(new Card(Color.BLUE, Value.ZERO));
        playableCards.add(new Card(Color.BLACK, Value.CHANGE));
        playableCards.add(new Card(Color.BLACK, Value.TAKEFOUR));
        Card card = unoAI.chooseCard(playableCards);
        Assert.assertEquals(card.getValue(),Value.EIGHT);
    }

    @Test
    public void playCard() {
        Card playableCard = new Card(Color.RED,Value.FIVE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.FIVE));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLUE, Value.SKIP));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        Card topCard = new Card(Color.BLACK,Value.CHANGE);
        Color chosenColor = Color.RED;
        Turn turn = unoAI.playCard(playableCard,hand,topCard,chosenColor);
        Assert.assertEquals(turn.getCard(),playableCard);
    }

    @Test
    public void playCardBlack() {
        Card playableCard = new Card(Color.BLACK,Value.CHANGE);
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.FIVE));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        Card topCard = new Card(Color.BLACK,Value.CHANGE);
        Color chosenColor = Color.RED;
        Turn turn = unoAI.playCard(playableCard,hand,topCard,chosenColor);
        Assert.assertEquals(turn.getCard(),playableCard);
        Assert.assertTrue(turn.getChoice().equals(Color.GREEN) || turn.getChoice().equals(Color.YELLOW));
    }

    @Test
    public void playBlackCard() {
        List<Card> hand = new ArrayList<>();
        hand.add(new Card(Color.RED, Value.FIVE));
        hand.add(new Card(Color.GREEN, Value.EIGHT));
        hand.add(new Card(Color.YELLOW, Value.EIGHT));
        hand.add(new Card(Color.RED, Value.SKIP));
        hand.add(new Card(Color.GREEN, Value.TAKETWO));
        hand.add(new Card(Color.YELLOW, Value.FOUR));
        hand.add(new Card(Color.BLACK, Value.CHANGE));
        hand.add(new Card(Color.BLACK, Value.TAKEFOUR));
        Card topCard = new Card(Color.BLACK,Value.CHANGE);
        Color chosenColor = Color.RED;
        Color color = unoAI.playBlackCard(hand,topCard,chosenColor);
        Assert.assertTrue(color.equals(Color.YELLOW) || color.equals(Color.GREEN));
    }

    @Test
    public void chooseColorBothNull() {
        Color color = unoAI.chooseColor(null,null);
        Assert.assertNotEquals(Color.BLACK,color);
    }

    @Test
    public void chooseColorListNull() {
        Color color = unoAI.chooseColor(Color.RED,null);
        Assert.assertTrue(color.equals(Color.GREEN) || color.equals(Color.BLUE) || color.equals(Color.YELLOW));
    }

    @Test
    public void chooseColorColorNull() {
        List<Color> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        Color color = unoAI.chooseColor(null,colors);
        Assert.assertTrue(color.equals(Color.RED) || color.equals(Color.BLUE));
    }

    @Test
    public void chooseColor() {
        List<Color> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        Color color = unoAI.chooseColor(Color.GREEN,colors);
        Assert.assertTrue(color.equals(Color.RED) || color.equals(Color.BLUE));
    }
}
