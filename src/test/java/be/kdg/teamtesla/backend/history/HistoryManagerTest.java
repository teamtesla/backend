package be.kdg.teamtesla.backend.history;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.card.Value;
import be.kdg.teamtesla.backend.game.Game;
import be.kdg.teamtesla.backend.game.GameManager;
import be.kdg.teamtesla.backend.game.GameService;
import be.kdg.teamtesla.backend.lobby.LobbyController;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HistoryManagerTest {

    @Autowired
    private HistoryManager historyManager;
    @Autowired
    private LobbyController lobbyManager;
    @Autowired
    private GameService gameService;
    @Autowired
    private GameManager gameManager;

    private final long userId1=1L;

    @Test
    @Transactional
    public void createHistory() {
        Lobby lobby = lobbyManager.createLobby("createGameWith2Players",userId1);
        long gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        History history = historyManager.createHistory(game);
        Assert.assertNotNull(history);
    }

    @Test
    @Transactional
    public void createTurn() {
        Lobby lobby = lobbyManager.createLobby("createGameWith2Players",userId1);
        long gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        History history = historyManager.createHistory(game);
        history.addTurn(historyManager.createTurn(new Card(Color.RED, Value.EIGHT),0,false,history));
        Assert.assertNotNull(history.getTurns().get(0));
        Assert.assertEquals(history.getPlayers().get(0).getHistoryPlayerId(),history.getTurns().get(0).getPlayer());
    }
}
