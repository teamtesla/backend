package be.kdg.teamtesla.backend.friend;

import be.kdg.teamtesla.backend.friend.dto.FriendRequestDTO;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.security.TokenProvider;
import be.kdg.teamtesla.backend.security.UnoUserService;
import be.kdg.teamtesla.backend.security.payload.AuthResponse;
import be.kdg.teamtesla.backend.security.payload.LoginRequest;
import be.kdg.teamtesla.backend.webSockets.WebSocketBaseMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class FriendControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private FriendManager friendManager;
    @Autowired
    private WebsocketDispatcher dispatcher;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UnoUserService unoUserService;

    private static String tokenA = null;
    private static String tokenB = null;

    @Test
    public void A_getTokenA() throws Exception {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("jvanhoye@hotmail.com");
        loginRequest.setPassword("Abc123456");
        MvcResult result = mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(loginRequest.toString()))
                .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(result.getResponse().getContentAsString(),AuthResponse.class);
        tokenA = authResponse.getAccessToken();
    }
    @Test
    public void B_getTokenB() throws Exception{
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("jan.vanhoye@student.kdg.be");
        loginRequest.setPassword("Abc123456");
        MvcResult result = mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(loginRequest.toString()))
                .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(result.getResponse().getContentAsString(),AuthResponse.class);
        tokenB = authResponse.getAccessToken();
    }
    @Test
    public void C_undoFriendsAndInvites(){
        unoUserService.findAll().forEach(u ->{
            u.getFriends().clear();
            unoUserService.saveUser(u);
        });
        friendManager.deleteAllFriendRequests();
    }
    @Test
    public void D_sendFriendInvite() throws Exception{
        FriendRequestDTO friendRequestDTO = new FriendRequestDTO();
        friendRequestDTO.setReceiver("jan.vanhoye@student.kdg.be");
        WebSocketBaseMessage wsm = new WebSocketBaseMessage();
        ObjectMapper objectMapper = new ObjectMapper();
        wsm.setObject(objectMapper.writeValueAsString(friendRequestDTO));
        wsm.setToken(tokenA);
        dispatcher.SendMessage("friend/request/make",wsm);
    }
    @Test
    public void E_receiveFriendInvites() throws Exception{
        WebSocketBaseMessage wbm = new WebSocketBaseMessage();
        wbm.setToken(tokenB);
        wbm.setObject("");
        dispatcher.SendMessage("friend/request/list",wbm);
        await().atMost(10, SECONDS).until(()->friendManager.getAllFriendRequests(unoUserService.findById(tokenProvider.getUserIdFromToken(tokenB))).size() >0);
    }
//    @Test
//    public void F_acceptFriendInvite() throws Exception{
//        WebSocketBaseMessage wbm = new WebSocketBaseMessage();
//        wbm.setToken(tokenB);
//        ObjectMapper objectMapper = new ObjectMapper();
//        FriendRequestDTO friendRequestDTO = new FriendRequestDTO();
//        friendRequestDTO.setReceiver("jvanhoye@hotmail.com");
//
//        wbm.setObject(objectMapper.writeValueAsString(friendRequestDTO));
//
//        dispatcher.SendMessage("friend/request/accept",wbm);
//        await().atMost(10,SECONDS).until(()->unoUserService.findById(tokenProvider.getUserIdFromToken(tokenB)).getFriends().size() > 0);
//    }
}
