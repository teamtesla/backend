package be.kdg.teamtesla.backend.lobby;

import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.PlayerRepo;
import be.kdg.teamtesla.backend.security.model.UnoUser;
import be.kdg.teamtesla.backend.security.persistence.UserRepo;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class LobbyRepoTest {
    @Autowired
    private LobbyRepo lobbyRepo;
    @Autowired
    private PlayerRepo playerRepo;
    @Autowired
    private UserRepo userRepo;
    private static long id;

    @Test
    public void A_CreateLobby(){
        UnoUser unoUser = new UnoUser();
        unoUser.setEmail("yannisarnouts");
        unoUser.setPassword("yannisarnouts");
        //userRepo.save(unoUser);
        Lobby lobby=new Lobby("TestLobbyA",unoUser);
        lobby=lobbyRepo.save(lobby);
        id=lobby.getLobbyId();
        assert true;
    }

    @Test
    public void B_IsLobbyCreated(){
        Lobby lobby =lobbyRepo.findByLobbyId(id);
        assert lobby != null;
    }
    @Test
    public void C_RemoveLobby(){
        lobbyRepo.delete(lobbyRepo.findByLobbyId(id));
        Lobby lobby = lobbyRepo.findByLobbyId(id);
        assert lobby == null;
    }
}
