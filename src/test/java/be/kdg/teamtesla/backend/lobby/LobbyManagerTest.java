package be.kdg.teamtesla.backend.lobby;

import be.kdg.teamtesla.backend.security.payload.AuthResponse;
import be.kdg.teamtesla.backend.security.payload.LoginRequest;
import be.kdg.teamtesla.backend.webSockets.WebSocketBaseMessage;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class LobbyManagerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LobbyService lobbyService;

    @Autowired
    private LobbyController lobbyManager;

    @Autowired
    private WebsocketDispatcher websocketDispatcher;
    private static String token = null;

    private static long lobbyId;

    @Test
    public void A_getToken() throws Exception {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("jvanhoye@hotmail.com");
        loginRequest.setPassword("Abc123456");
        MvcResult result = mockMvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(loginRequest.toString()))
                .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        AuthResponse authResponse = mapper.readValue(result.getResponse().getContentAsString(),AuthResponse.class);
        token = authResponse.getAccessToken();
    }

    @Test
    @Transactional
    public void B_createLobby(){

        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebSocketBaseMessage webSocketBaseMessage = new WebSocketBaseMessage(token,"{\"name\":\"lobbyA\"}");

        websocketDispatcher.SendMessage("lobby/create", webSocketBaseMessage);
        System.out.println(lobbyService.getLobbies());
        await().atMost(20, SECONDS).until(() -> lobbyService.getLobbies().size() > 0);
    }

//    @Test
//    public void C_createLobby() {
//        Lobby lobby = lobbyManager.createLobby("LobbyManagerTest",22L);
//        lobbyId = lobby.getLobbyId();
//        System.out.println("createLobby: " + lobbyId);
//        Assert.assertNotNull(lobbyService.getLobbyById(lobbyId));
//    }
//
//    @Test
//    public void D_addUser() {
//        lobbyManager.addUser(lobbyId,21L);
//        int size = lobbyService.getCurrentUserAmount(lobbyId);
//        System.out.println("addUser: " + size);
//        Assert.assertEquals(2,size);
//    }
//
//    @Test
//    public void E_createGame() {
//        System.out.println(lobbyManager.createGame(lobbyId));
//        int size = lobbyService.getCurrentGameAmount(lobbyId);
//        System.out.println("createGame: " + size);
//        Assert.assertEquals(1, size);
//    }
//
//    @Test
// public void Z_emptyLobbies() {
//        lobbyService.removeLobbies();
//       int size = lobbyService.getLobbies().size();
//       Assert.assertEquals(0,size);
//    }
//    @Test
//    @Transactional
//    public void C_getLobbiesByUser(){
//        List<Lobby> lobbylist = lobbyService.getLobbyByUser(21);
//        System.out.println(lobbylist);
//    }
}
