package be.kdg.teamtesla.backend.game;

import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.CardService;
import be.kdg.teamtesla.backend.card.Color;
import be.kdg.teamtesla.backend.card.Value;
import be.kdg.teamtesla.backend.game.exception.CardNotInHandException;
import be.kdg.teamtesla.backend.game.exception.NoColorException;
import be.kdg.teamtesla.backend.game.exception.UnplayableCardException;
import be.kdg.teamtesla.backend.game.exception.WrongUserException;
import be.kdg.teamtesla.backend.lobby.LobbyController;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class PlayCardTest {

    @Autowired
    private GameService gameService;
    @Autowired
    private GameManager gameManager;
    @Autowired
    private LobbyController lobbyManager;
    @Autowired
    private CardService cardService;

    private static long gameId;

    private final long userId1=1L;
    private final long userId2=2L;

    @Test
    @Transactional
    public void E_DrawACard() {
        Lobby lobby = lobbyManager.createLobby("playATurn",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        game.setTurn(0);
        game = gameService.createGame(game);
        int turn = game.getTurn();
        long userId = game.getPlayers().get(turn).getUnoUser().getId();
        int start = game.getPlayers().get(turn).getHand().size();
        gameManager.playATurn(gameId,userId,999,null,false);
        Assert.assertEquals(start+1,gameService.getGameById(gameId).getPlayers().get(turn).getHand().size());
    }

    @Test
    @Transactional
    public void F_playALegalCard(){
        Lobby lobby = lobbyManager.createLobby("playACard",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of Skip
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        System.out.println(game.getDiscard().get(game.getDiscard().size()-1).getColor());
        //Put Green 9 in player's hand
        Card green9=cardService.getCardById(69L);
        game.getPlayers().get(0).getHand().add(green9);
        int initialSize=game.getPlayers().get(0).getHand().size();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),69L,null,false);

        System.out.println(game.getDiscard().get(game.getDiscard().size()-1).getColor());
        //Asserting that the player lost a card
        Assert.assertEquals(initialSize-1,game.getPlayers().get(0).getHand().size());
        //Asserting that a green 10 is on top of the pile
        Assert.assertEquals(game.getDiscard().get(game.getDiscard().size()-1).hashCode(),cardService.getCardById(69L).hashCode());
    }

    @Test
    @Transactional
    public void G_playAnIllegalCard(){
        Lobby lobby = lobbyManager.createLobby("playAnIllegalCard",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of Skip
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Green 5 in player's hand if he doesn't have
        Card green5=cardService.getCardById(65L);
        game.getPlayers().get(0).getHand().add(green5);
        int initialSize=game.getPlayers().get(0).getHand().size();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),65L,null,false);
        Assert.assertEquals(initialSize,game.getPlayers().get(0).getHand().size());
    }

    @Test(expected = WrongUserException.class)
    @Transactional
    public void H_playingOffTurn(){
        Lobby lobby = lobbyManager.createLobby("playingOffTurn",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of Skip
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Green 9 in player's hand
        game.getPlayers().get(1).getHand().add(cardService.getCardById(69L));
        int sizeBeforeTurn=game.getPlayers().get(1).getHand().size();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(1).getUnoUser().getId(),69L,null,false);
        Assert.assertEquals(sizeBeforeTurn,game.getPlayers().get(1).getHand().size());
        Assert.assertEquals(0,game.getTurn());
    }

    @Test
    @Transactional
    public void I_playingAnAdd2(){
        Lobby lobby = lobbyManager.createLobby("playingAdd2",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Blue add2 in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(50L));
        int sizeDeckBeforeTurn=game.getDeck().size();
        int sizePlayer2BeforeTurn=game.getPlayers().get(1).getHand().size();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),50L,null,false);
        Assert.assertEquals(game.getDiscard().get(2).getValue(), Value.TAKETWO);
        Assert.assertEquals(sizeDeckBeforeTurn-2,game.getDeck().size());
        Assert.assertEquals(sizePlayer2BeforeTurn+2,game.getPlayers().get(1).getHand().size());


    }

    @Test
    @Transactional
    public void J_playingASkip(){
        Lobby lobby = lobbyManager.createLobby("playingAdd2",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Blue Skip  in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(48L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),48L,null,false);
        Assert.assertEquals(game.getDiscard().get(2).getValue(), Value.SKIP);
        Assert.assertEquals(2,game.getTurn());
    }


    @Test(expected = CardNotInHandException.class)
    @Transactional
    public void K_playingCardNotInHand(){
        Lobby lobby = lobbyManager.createLobby("playingWhatILike",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //See if this Blue Skip is in player's hand
        game.getPlayers().get(0).getHand().remove(cardService.getCardById(48L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),48L,null,false);

    }



    @Test
    @Transactional
    public void L_playingReverse(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId1);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //See if this Blue Reverse is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(46L));
        System.out.println(cardService.getCardById(50L).getValue());
        boolean initialDirection=game.isClockwise();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),46L,null,false);
        Assert.assertEquals(game.getDiscard().get(2).getValue(), Value.REVERSE);
        Assert.assertEquals(!initialDirection,game.isClockwise());
    }

    @Test(expected = NoColorException.class)
    @Transactional
    public void M_playingAdd4WithoutColor(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //See if this Black +4 is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(107L));
        int initialSizeNextPlayer=game.getPlayers().get(1).getHand().size();
        int initialTurn=game.getTurn();
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),107L,null,false);
        Assert.assertEquals(initialSizeNextPlayer,game.getPlayers().get(1).getHand().size());
        Assert.assertEquals(initialTurn,game.getTurn());
    }

    @Test
    @Transactional
    public void N_playingAdd4(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //See if this Black +4 is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(107L));
        int initialSizeNextPlayer=game.getPlayers().get(1).getHand().size();
        Optional<Game> optionalGame = gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),107L, Color.RED,false);
        if (optionalGame.isPresent()) {
            Game playedGame = optionalGame.get();
            Assert.assertEquals(initialSizeNextPlayer+4,playedGame.getPlayers().get(1).getHand().size());
        } else Assert.fail();

    }

    @Test
    @Transactional
    public void O_playingChange(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        game.setColor(Color.BLUE);
        //See if this Black Change is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(102L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),102L, Color.RED,false);
        Assert.assertEquals(Color.RED,game.getColor());
    }

    @Test
    @Transactional
    public void O2_playingOnChangeCard(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        game.setColor(Color.BLUE);
        //See if this Black Change is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(102L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),102L, Color.RED,false);

        game.getPlayers().get(1).getHand().add(cardService.getCardById(10L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(1).getUnoUser().getId(),10L, null,false);

        Assert.assertEquals(game.getDiscard().get(game.getDiscard().size()-1),cardService.getCardById(10L));
    }

    @Test
    @Transactional
    public void P_playingChange(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        game.setColor(Color.BLUE);
        //See if this Black Change is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(102L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),102L, Color.RED,false);
        Assert.assertEquals(Color.RED,game.getColor());
    }

    @Test(expected = UnplayableCardException.class)
    @Transactional
    public void Q_playingWrongOntoChange(){
        Lobby lobby = lobbyManager.createLobby("playingReverse",userId2);
        long lobbyId = lobby.getLobbyId();
        lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        //Setting turn back to first player for testing purposes in case of SkipStart
        game.setTurn(0);
        game.setClockwise(true);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        game.setColor(Color.BLUE);
        //See if this Black Change is in player's hand
        game.getPlayers().get(0).getHand().add(cardService.getCardById(102L));
        //give next player blue 8
        game.getPlayers().get(1).getHand().add(cardService.getCardById(43L));
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(0).getUnoUser().getId(),102L, Color.RED,false);
        gameManager.playATurn(game.getGameId(),game.getPlayers().get(1).getUnoUser().getId(),43L, null,false);
    }




}
