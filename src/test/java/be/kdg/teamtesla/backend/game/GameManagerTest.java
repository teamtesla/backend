package be.kdg.teamtesla.backend.game;


import be.kdg.teamtesla.backend.card.Card;
import be.kdg.teamtesla.backend.card.CardService;
import be.kdg.teamtesla.backend.card.Value;
import be.kdg.teamtesla.backend.history.HistoryManager;
import be.kdg.teamtesla.backend.lobby.LobbyController;
import be.kdg.teamtesla.backend.lobby.LobbyService;
import be.kdg.teamtesla.backend.lobby.model.Lobby;
import be.kdg.teamtesla.backend.player.Player;
import be.kdg.teamtesla.backend.player.PlayerService;
import be.kdg.teamtesla.backend.security.UserDetailServiceImpl;
import be.kdg.teamtesla.backend.webSockets.WebsocketDispatcher;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //Tests run in alphabetical order.
@AutoConfigureMockMvc
public class GameManagerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GameService gameService;
    @Autowired
    private GameManager gameManager;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private LobbyController lobbyManager;
    @Autowired
    private CardService cardService;
    @Autowired
    private UserDetailServiceImpl userDetailService;
    @Autowired
    private WebsocketDispatcher websocketDispatcher;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private HistoryManager historyManager;
    private static long gameId;

    private final long userId1=2L;
    private final long userId2=3L;

    @Test
    public void A_CreateGame(){
        int initialSize = gameService.getGames().size();
        Lobby lobby = lobbyManager.createLobby("createGame",userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        List<Game> games = gameService.getGames();
        System.out.println(games);
        Assert.assertEquals(initialSize+1,games.size());
    }

    @Test
    @Transactional
    public void B_AddDiscard(){
        Game game = gameService.getGameById(gameId);
        game.getDiscard().add(gameManager.drawDiscardCard(game.getDeck(),game));
        Assert.assertEquals(2,game.getDiscard().size());
    }

/* CalledUno in player fucks these tests up
    @Test
    public void B_DeleteAllGames() {
        gameService.removeGames();
        Assert.assertEquals(0,gameService.getGames().size());
    }

    @Test
    public void B_DeleteAllLobbies() {
        lobbyService.removeLobbies();
        Assert.assertEquals(0,lobbyService.getLobbies().size());
    }
*/
    @Test
    @Transactional
    public void D_CreateGameWith2Players() {
        int initialSize = gameService.getGames().size();
        Lobby lobby = lobbyManager.createLobby("createGameWith2Players",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameManager.createGame(lobby).getGameId();
        List<Game> games = gameService.getGames();
        System.out.println(games);
        Assert.assertEquals(initialSize+1,games.size());
    }

    @Test
    @Transactional
    public void E_playersHandsFilled() {
        Lobby lobby = lobbyManager.createLobby("handsFilled",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        List<Player> players=game.getPlayers();
        for (Player player : players) {
            Assert.assertTrue(player.getHand().size()>=7);
        }
    }

    @Test
    @Transactional
    public void F_FinishAGame(){
        Lobby lobby = lobbyManager.createLobby("finishAGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game = gameService.getGameById(gameId);
        game.setTurn(0);
        //Blue 9 on discard top
        game.getDiscard().add(cardService.getCardById(44L));
        //Blue 8 as last card in player 1 hand
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        game.getPlayers().get(0).getHand().add(cardService.getCardById(43L));

        gameManager.playATurn(gameId,game.getPlayers().get(0).getUnoUser().getId(),43L,null,true);
        Assert.assertEquals(true,game.isFinished());
    }
    @Test
    @Transactional
    public void G_RefillDeck(){
        Lobby lobby = lobbyManager.createLobby("finishAGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        Game game = gameManager.createGame(lobby);
        gameId = game.getGameId();
        game.setTurn(0);
        //Fill up discard
        game.getDiscard().removeAll(game.getDiscard());
        game.getDiscard().add(cardService.getCardById(44L));
        game.getDiscard().add(cardService.getCardById(45L));
        game.getDiscard().add(cardService.getCardById(userId1));
        game.getDiscard().add(cardService.getCardById(47L));
        game.getDiscard().add(cardService.getCardById(48L));
        game.getDiscard().add(cardService.getCardById(49L));
        game.getDiscard().add(cardService.getCardById(50L));
        game.getDiscard().add(cardService.getCardById(51L));
        //Empty Deck except one
        game.getDeck().removeAll(game.getDeck());
        game.getDeck().add(cardService.getCardById(70L));
        gameManager.playATurn(gameId,game.getPlayers().get(0).getUnoUser().getId(),999L,null,false);
        List<Card> DiscardDeck =game.getDiscard();
        for (Card card : DiscardDeck) {
            System.out.println(card.getCardId());
        }
        Assert.assertEquals(7,game.getDeck().size());
        Assert.assertEquals(1,game.getDiscard().size());
    }

    @Test
    @Transactional
    public void H_InitialAdd2(){
        Lobby lobby = lobbyManager.createLobby("initialGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game=gameService.getGameById(gameId);
        Method firstTurnMethod=null;
        int handSizeBeforeStart=game.getPlayers().get(0).getHand().size();
        try{ firstTurnMethod=GameManager.class.getDeclaredMethod("initialCard",Game.class);
        game.setTurn(0);
        //Red Add 2 on discard top
        game.getDiscard().add(cardService.getCardById(25L));
        firstTurnMethod.setAccessible(true);
        firstTurnMethod.invoke(gameManager,game);
        }catch (Exception e){e.printStackTrace();}
        Assert.assertEquals(Value.TAKETWO,game.getTopDiscardCard().getValue());
        Assert.assertEquals(1,game.getTurn());
        Assert.assertEquals(handSizeBeforeStart+2,game.getPlayers().get(0).getHand().size());
    }

    @Test
    @Transactional
    public void I_InitialSkip(){
        Lobby lobby = lobbyManager.createLobby("initialGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game=gameService.getGameById(gameId);
        Method firstTurnMethod=null;
        int handSizeBeforeStart=game.getPlayers().get(0).getHand().size();
        try{ firstTurnMethod=GameManager.class.getDeclaredMethod("initialCard",Game.class);
            game.setTurn(0);
            game.setClockwise(true);
            //Red Skip on discard top
            game.getDiscard().add(cardService.getCardById(23L));
            firstTurnMethod.setAccessible(true);
            firstTurnMethod.invoke(gameManager,game);
        }catch (Exception e){e.printStackTrace();}
        Assert.assertEquals(Value.SKIP,game.getTopDiscardCard().getValue());
        Assert.assertEquals(1,game.getTurn());
    }


    @Test
    @Transactional
    public void KK_GameInitializedProperly(){
        Lobby lobby = lobbyManager.createLobby("initialGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game=gameService.getGameById(gameId);
        List<Player> players=game.getPlayers();
        for (Player player : players) {
            Assert.assertTrue(player.getHand().size()>6);
        }
        Assert.assertFalse(game.isFinished());
        Assert.assertEquals(1,game.getDiscard().size());
        Assert.assertEquals(lobby,game.getLobby());
    }

    @Test
    @Transactional
    public void J_PlayerDidNotCallUno(){
        Lobby lobby = lobbyManager.createLobby("initialGame",userId1);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId2);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game=gameService.getGameById(gameId);
        game.setTurn(0);
        //Empty player 1 hand
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        //Blue 9 on discard top
        game.getDiscard().remove(0);
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Green5 and Green 9 in player's hand
        Card green9=cardService.getCardById(69L);
        game.getPlayers().get(0).getHand().add(green9);
        Card green5=cardService.getCardById(65L);
        game.getPlayers().get(0).getHand().add(green5);
        int initialHandsize=game.getPlayers().get(0).getHand().size();
        Assert.assertEquals(2,initialHandsize);
        //Play a card without calling uno
        gameManager.playATurn(gameId,game.getPlayers().get(0).getUnoUser().getId(),69L,null,false);
        Assert.assertEquals(initialHandsize+1,game.getPlayers().get(0).getHand().size());
    }

    @Test
    @Transactional
    public void K_PlayerDidCallUno() throws Exception{
        Lobby lobby = lobbyManager.createLobby("initialGame",userId2);
        long lobbyId = lobby.getLobbyId();
        lobby = lobbyManager.addUser(lobbyId,userId1);
        gameId = gameManager.createGame(lobby).getGameId();
        Game game=gameService.getGameById(gameId);
        game.setTurn(0);
        //Empty player 1 hand
        game.getPlayers().get(0).getHand().removeAll(game.getPlayers().get(0).getHand());
        //Blue 9 on discard top
        game.getDiscard().remove(0);
        game.getDiscard().add(cardService.getCardById(44L));
        //Put Green5 and Green 9 in player's hand
        Card green9=cardService.getCardById(69L);
        game.getPlayers().get(0).getHand().add(green9);
        Card green5=cardService.getCardById(65L);
        game.getPlayers().get(0).getHand().add(green5);
        int initialHandsize=game.getPlayers().get(0).getHand().size();
        Assert.assertEquals(2,initialHandsize);
        gameManager.playATurn(gameId,game.getPlayers().get(0).getUnoUser().getId(),69L,null,true);
        Assert.assertEquals(initialHandsize-1,game.getPlayers().get(0).getHand().size());
    }

    @Test
    @Transactional
    public void L_AIPlayers() {
        Lobby lobby = lobbyManager.createLobby("initialGame",userId2);
        Game game = new Game();
        game.setLobby(lobby);
        game.setPlayers(new ArrayList<>());
        game.setTurn(0);
        game.setDeck(cardService.getAllCards());
        game.setDiscard(new ArrayList<>());
        game.setClockwise(true);
        for (int i = 0; i < 4; i++) {
            game.getPlayers().add(new Player());
        }
        game.getDiscard().add(gameManager.drawDiscardCard(game.getDeck(),game));
        for (Player player:game.getPlayers()) {
            game.setAmount(game.getAmount()+1);
            player.setGame(game);
            for (int i = 0; i < 7; i++) {
                player.getHand().add(gameManager.drawACard(game.getDeck(),game));
            }
        }
        gameManager.initialCard(game);
        game.getPlayers().get(game.getTurn()).setTurn(true);
        game.setHistory(historyManager.createHistory(game));
        game = gameService.createGame(game);
        Optional<Game> optionalGame = gameManager.checkComputer(game);
        Optional<Game> previous = optionalGame;
        while (optionalGame.isPresent()) {
            previous = optionalGame;
            System.out.println(optionalGame.get().getTopDiscardCard().getColor() + " : " + optionalGame.get().getTopDiscardCard().getValue() + ", deck: " + optionalGame.get().getDeck().size());
            optionalGame = gameManager.checkComputer(optionalGame.get());
        }
        Assert.assertTrue(previous.isPresent() && previous.get().isFinished());
    }
}
